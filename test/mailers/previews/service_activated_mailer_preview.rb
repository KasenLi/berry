# Preview all emails at http://localhost:3000/rails/mailers/service_activated_mailer
class ServiceActivatedMailerPreview < ActionMailer::Preview
	def sample_mail_preview

		operator = {
			name: "Juan",
			email: "kasenkelvin.10@gmail.com"
		}

		units = ["kasen", "miguel"]
		ServiceActivatedMailer.send_mail(units, operator, "Cambio de aceite")
	end
end
