class CreateBackendMqttLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_mqtt_logs do |t|
    	t.string :server, index: true, null: false, default: 'main'
      t.string :service, index: true, null: false, default: ''
      t.integer :code, index: true, null: false, default: 200
      t.string :message, index: true
      t.hstore :error, null: false, default: {}
      t.hstore :information, null: false, default: {}

      t.timestamps
    end

    add_index(:backend_mqtt_logs, [:created_at, :server, :service], order: { created_at: :DESC, server: :ASC, service: :ASC })
  end
end
