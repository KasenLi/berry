class CreateBackendEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_events do |t|

      t.string :unit, null: false
      t.float :lat
      t.float :lon
      t.integer :speed
      t.integer :course
      t.integer :alt
      t.string :address
      t.integer :sats, default: 255
      t.string :event_code, null: false
      t.string :event_description
      t.datetime :date_receive
      t.timestamps
    end
    add_index :backend_events, [:unit,:created_at]
  end
end
