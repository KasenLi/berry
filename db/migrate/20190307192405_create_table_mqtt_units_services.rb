class CreateTableMqttUnitsServices < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_mqtt_units_services do |t|
    	t.belongs_to :mqtt_unit, index: true
    	t.belongs_to :service, index: true
    	t.float :mileage, default: 0.0
    	t.float :engine_hours, default: 0.0

    	t.timestamps
    end
  end
end
