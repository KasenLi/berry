class CreateBackendServiceRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_service_records do |t|
    	# Registro de servicios
    	t.string :name, null: false, default: ""
    	t.integer :unit_id, index: true, null: false
    	t.integer :state, null: false, default: 0
    	t.datetime :date, null: false

      # => Relation with external DBs
      t.integer :operator_id, index: true, null: false
      t.integer :customer_id, index: true, null: false
      
      # => Deleted fields
      t.boolean :deleted, default: false, index: true
      t.datetime :deleted_at, index: true
      
      t.timestamps
    end
  end
end
