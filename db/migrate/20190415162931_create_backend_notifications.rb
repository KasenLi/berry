class CreateBackendNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_notifications do |t|
    	t.string :name, null: false, default: ""
    	t.string :details, null: false, default: ""
    	t.datetime :date
    	t.boolean :watched, default: false, index: true

    	# => Relation with external DBs
      t.integer :operator_id, index: true, null: false
      t.integer :customer_id, index: true, null: false
      
      # => Deleted fields
      t.boolean :deleted, default: false, index: true
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
