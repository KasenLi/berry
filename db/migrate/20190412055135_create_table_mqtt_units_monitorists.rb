class CreateTableMqttUnitsMonitorists < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_monitorists_mqtt_units do |t|
    	t.belongs_to :mqtt_unit, index: true
    	t.belongs_to :monitorist, index: true

    	t.timestamps
    end
  end
end
