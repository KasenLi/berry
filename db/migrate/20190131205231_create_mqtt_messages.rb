class CreateMqttMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :mqtt_messages do |t|
        enable_extension 'hstore'
    	t.string :ident, index: true, null: false
    	t.hstore :position, null: false, default: {}
    	t.hstore :sensors, null: false, default: {}
        t.float :mileage, default: 0.0
    	t.datetime :date_receive, null: false
    	t.timestamps
    end

    add_index(:mqtt_messages, [:ident, :date_receive], order: { date_receive: :DESC, index: :ASC })
  end
end
