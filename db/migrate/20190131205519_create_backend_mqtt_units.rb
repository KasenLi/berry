class CreateBackendMqttUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_mqtt_units do |t|
    	t.integer :wialon_id, null: false, index: true, unique: true
      t.string :wialon_name, null: false
      t.string :wialon_economic, null: false, default: ""
      t.string :wialon_vin, null: false, default: ""
      t.string :wialon_plate, null: false, default: ""
      t.string :wialon_imei, null: false, index: true, unique: true

      t.integer :linked_wialon_account_id, null: false, index: true
      t.integer :operator_id, null: false, index: true
      t.integer :customer_id, null: false, index: true

      t.boolean :deleted, index: true, default: false
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
