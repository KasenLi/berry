class CreateBackendWorkshops < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_workshops do |t|

    	t.string :name, null: false, default: ""
    	t.string :service, null: false, default: ""
    	t.datetime :date_in
    	t.datetime :date_out
    	t.references :backend_mqtt_unit, foreign_key: true
    	t.integer :state, null: false, default: 0

    	# => Relation with external DBs
      t.integer :operator_id, index: true, null: false
      t.integer :customer_id, index: true, null: false
      
      # => Deleted fields
      t.boolean :deleted, default: false, index: true
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
