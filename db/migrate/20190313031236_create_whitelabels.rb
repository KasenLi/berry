class CreateWhitelabels < ActiveRecord::Migration[5.2]
  def change
    create_table :whitelabels do |t|
    	t.references :goffice_customer, index: true
      t.string :brand, null: false, index: true
      t.string :cname_record, null: false, unique: true
      t.integer :app, null: false, index: true, default: 0
      t.string :color, null: false, index: true
      t.text :logo, null: false
      t.string :page_title, null: false, index: true
      t.string :footer_url, null: false, index: true
      t.string :footer_name, null: false, index: true
      t.boolean :approved, null: false, index: true, default: false
      t.timestamps
    end
  end
end
