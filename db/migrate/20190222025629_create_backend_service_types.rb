class CreateBackendServiceTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_service_types do |t|
    	t.string :name, null: false, default: ""

    	# => Relation with external DBs
      t.integer :operator_id, index: true, null: false
      t.integer :customer_id, index: true, null: false
      
      # => Deleted fields
      t.boolean :deleted, default: false, index: true
      t.datetime :deleted_at, index: true
    	
    	# Grupo de unidades
      t.timestamps
    end
  end
end
