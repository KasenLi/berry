class CreateBackendParameters < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_parameters do |t|

    	t.string :name
      t.string :es_name
      t.integer :chart_type, default: 0
      t.string :measure
      t.boolean :modal, default: false
      t.integer :modal_type, default: 0
      t.boolean :extra_chart, default: true
      t.integer :extra_chart_type, default: 0
      t.hstore :lim_legends, default: {}
      t.string :icon
      
      # => Relation with external DBs
      t.integer :operator_id, index: true, null: false
      t.integer :customer_id, index: true, null: false
      
      # => Deleted fields
      t.boolean :deleted, default: false, index: true
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
