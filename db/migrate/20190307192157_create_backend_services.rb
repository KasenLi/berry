class CreateBackendServices < ActiveRecord::Migration[5.2]
  def change
    create_table :backend_services do |t|
    	# Servicios
    	t.string :name, null: false, default: ""
    	t.integer :frequency_km, null: false, default: 0
    	t.integer :frequency_time, null: false, default: 0
    	t.integer :time_type, null: false, default: 0
    	t.integer :frequency_engine, null: false, default: 0
      t.boolean :activated, default: true
      t.datetime :activated_at, index: true

    	# => Relation with external DBs
      t.integer :operator_id, index: true, null: false
      t.integer :customer_id, index: true, null: false
      
      # => Deleted fields
      t.boolean :deleted, default: false, index: true
      t.datetime :deleted_at, index: true
      
      t.timestamps
    end
  end
end
