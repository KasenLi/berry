# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_20_173555) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"

  create_table "backend_events", force: :cascade do |t|
    t.string "unit", null: false
    t.float "lat"
    t.float "lon"
    t.integer "speed"
    t.integer "course"
    t.integer "alt"
    t.string "address"
    t.integer "sats", default: 255
    t.string "event_code", null: false
    t.string "event_description"
    t.datetime "date_receive"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unit", "created_at"], name: "index_backend_events_on_unit_and_created_at"
  end

  create_table "backend_monitorists", force: :cascade do |t|
    t.string "name", default: ""
    t.string "phone", default: ""
    t.string "username", default: ""
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_backend_monitorists_on_customer_id"
    t.index ["deleted"], name: "index_backend_monitorists_on_deleted"
    t.index ["deleted_at"], name: "index_backend_monitorists_on_deleted_at"
    t.index ["email"], name: "index_backend_monitorists_on_email", unique: true
    t.index ["operator_id"], name: "index_backend_monitorists_on_operator_id"
    t.index ["reset_password_token"], name: "index_backend_monitorists_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_backend_monitorists_on_unlock_token", unique: true
  end

  create_table "backend_monitorists_mqtt_units", force: :cascade do |t|
    t.bigint "mqtt_unit_id"
    t.bigint "monitorist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["monitorist_id"], name: "index_backend_monitorists_mqtt_units_on_monitorist_id"
    t.index ["mqtt_unit_id"], name: "index_backend_monitorists_mqtt_units_on_mqtt_unit_id"
  end

  create_table "backend_mqtt_logs", force: :cascade do |t|
    t.string "server", default: "main", null: false
    t.string "service", default: "", null: false
    t.integer "code", default: 200, null: false
    t.string "message"
    t.hstore "error", default: {}, null: false
    t.hstore "information", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_backend_mqtt_logs_on_code"
    t.index ["created_at", "server", "service"], name: "index_backend_mqtt_logs_on_created_at_and_server_and_service", order: { created_at: :desc }
    t.index ["message"], name: "index_backend_mqtt_logs_on_message"
    t.index ["server"], name: "index_backend_mqtt_logs_on_server"
    t.index ["service"], name: "index_backend_mqtt_logs_on_service"
  end

  create_table "backend_mqtt_units", force: :cascade do |t|
    t.integer "wialon_id", null: false
    t.string "wialon_name", null: false
    t.string "wialon_economic", default: "", null: false
    t.string "wialon_vin", default: "", null: false
    t.string "wialon_plate", default: "", null: false
    t.string "wialon_imei", null: false
    t.integer "linked_wialon_account_id", null: false
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_backend_mqtt_units_on_customer_id"
    t.index ["deleted"], name: "index_backend_mqtt_units_on_deleted"
    t.index ["deleted_at"], name: "index_backend_mqtt_units_on_deleted_at"
    t.index ["linked_wialon_account_id"], name: "index_backend_mqtt_units_on_linked_wialon_account_id"
    t.index ["operator_id"], name: "index_backend_mqtt_units_on_operator_id"
    t.index ["wialon_id"], name: "index_backend_mqtt_units_on_wialon_id"
    t.index ["wialon_imei"], name: "index_backend_mqtt_units_on_wialon_imei"
  end

  create_table "backend_mqtt_units_services", force: :cascade do |t|
    t.bigint "mqtt_unit_id"
    t.bigint "service_id"
    t.float "mileage", default: 0.0
    t.float "engine_hours", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mqtt_unit_id"], name: "index_backend_mqtt_units_services_on_mqtt_unit_id"
    t.index ["service_id"], name: "index_backend_mqtt_units_services_on_service_id"
  end

  create_table "backend_notifications", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "details", default: "", null: false
    t.datetime "date"
    t.boolean "watched", default: false
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_backend_notifications_on_customer_id"
    t.index ["deleted"], name: "index_backend_notifications_on_deleted"
    t.index ["deleted_at"], name: "index_backend_notifications_on_deleted_at"
    t.index ["operator_id"], name: "index_backend_notifications_on_operator_id"
    t.index ["watched"], name: "index_backend_notifications_on_watched"
  end

  create_table "backend_parameters", force: :cascade do |t|
    t.string "name"
    t.string "es_name"
    t.integer "chart_type", default: 0
    t.string "measure"
    t.boolean "modal", default: false
    t.integer "modal_type", default: 0
    t.boolean "extra_chart", default: true
    t.integer "extra_chart_type", default: 0
    t.hstore "lim_legends", default: {}
    t.string "icon"
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_backend_parameters_on_customer_id"
    t.index ["deleted"], name: "index_backend_parameters_on_deleted"
    t.index ["deleted_at"], name: "index_backend_parameters_on_deleted_at"
    t.index ["operator_id"], name: "index_backend_parameters_on_operator_id"
  end

  create_table "backend_service_records", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.integer "unit_id", null: false
    t.integer "state", default: 0, null: false
    t.datetime "date", null: false
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_backend_service_records_on_customer_id"
    t.index ["deleted"], name: "index_backend_service_records_on_deleted"
    t.index ["deleted_at"], name: "index_backend_service_records_on_deleted_at"
    t.index ["operator_id"], name: "index_backend_service_records_on_operator_id"
    t.index ["unit_id"], name: "index_backend_service_records_on_unit_id"
  end

  create_table "backend_service_types", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_backend_service_types_on_customer_id"
    t.index ["deleted"], name: "index_backend_service_types_on_deleted"
    t.index ["deleted_at"], name: "index_backend_service_types_on_deleted_at"
    t.index ["operator_id"], name: "index_backend_service_types_on_operator_id"
  end

  create_table "backend_services", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.integer "frequency_km", default: 0, null: false
    t.integer "frequency_time", default: 0, null: false
    t.integer "time_type", default: 0, null: false
    t.integer "frequency_engine", default: 0, null: false
    t.boolean "activated", default: true
    t.datetime "activated_at"
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activated_at"], name: "index_backend_services_on_activated_at"
    t.index ["customer_id"], name: "index_backend_services_on_customer_id"
    t.index ["deleted"], name: "index_backend_services_on_deleted"
    t.index ["deleted_at"], name: "index_backend_services_on_deleted_at"
    t.index ["operator_id"], name: "index_backend_services_on_operator_id"
  end

  create_table "backend_workshops", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "service", default: "", null: false
    t.datetime "date_in"
    t.datetime "date_out"
    t.bigint "backend_mqtt_unit_id"
    t.integer "state", default: 0, null: false
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["backend_mqtt_unit_id"], name: "index_backend_workshops_on_backend_mqtt_unit_id"
    t.index ["customer_id"], name: "index_backend_workshops_on_customer_id"
    t.index ["deleted"], name: "index_backend_workshops_on_deleted"
    t.index ["deleted_at"], name: "index_backend_workshops_on_deleted_at"
    t.index ["operator_id"], name: "index_backend_workshops_on_operator_id"
  end

  create_table "mqtt_messages", force: :cascade do |t|
    t.string "ident", null: false
    t.hstore "position", default: {}, null: false
    t.hstore "sensors", default: {}, null: false
    t.float "mileage", default: 0.0
    t.datetime "date_receive", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ident", "date_receive"], name: "index_mqtt_messages_on_ident_and_date_receive", order: { date_receive: :desc }
    t.index ["ident"], name: "index_mqtt_messages_on_ident"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name", default: "", null: false
    t.string "phone", default: "", null: false
    t.integer "operator_id", null: false
    t.integer "customer_id", null: false
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_users_on_customer_id"
    t.index ["deleted"], name: "index_users_on_deleted"
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["operator_id"], name: "index_users_on_operator_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "whitelabels", force: :cascade do |t|
    t.bigint "goffice_customer_id"
    t.string "brand", null: false
    t.string "cname_record", null: false
    t.integer "app", default: 0, null: false
    t.string "color", null: false
    t.text "logo", null: false
    t.string "page_title", null: false
    t.string "footer_url", null: false
    t.string "footer_name", null: false
    t.boolean "approved", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["app"], name: "index_whitelabels_on_app"
    t.index ["approved"], name: "index_whitelabels_on_approved"
    t.index ["brand"], name: "index_whitelabels_on_brand"
    t.index ["color"], name: "index_whitelabels_on_color"
    t.index ["footer_name"], name: "index_whitelabels_on_footer_name"
    t.index ["footer_url"], name: "index_whitelabels_on_footer_url"
    t.index ["goffice_customer_id"], name: "index_whitelabels_on_goffice_customer_id"
    t.index ["page_title"], name: "index_whitelabels_on_page_title"
  end

  add_foreign_key "backend_workshops", "backend_mqtt_units"
end
