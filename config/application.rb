require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ViggoBerry
  class Application < Rails::Application

  	@env = Rails.application.credentials
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.app_generators.javascript_engine :javascript

    config.generators do |g|
      g.scaffold_stylesheet = false
      g.stylesheets = false
      g.javascripts = false
    end

    config.i18n.load_path += Dir["#{Rails.root}/config/devise/*.{rb,yml}"]
    config.i18n.default_locale = ( @env.app_lang.nil? ? :es : @env.app_lang.to_sym )
    config.i18n.fallbacks = [:es, :en]
    
    config.time_zone = 'Caracas'
    config.active_record.default_timezone = :utc

    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')

    Slim::Engine.set_options pretty: true
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
