let path = require('path')

module.exports = {
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.resolve(__dirname, '../../../app/javascript'),
      'src': path.resolve(__dirname, '../../../app/javascript'),
      'asset$': path.resolve(__dirname, '../../../app/assets'),
      'i18n': path.resolve(__dirname, '../../locales'),
    },
    extensions: ['*', '.js', '.vue', '.json']
  }
}