const { environment } = require('@rails/webpacker')
const vue =  require('./loaders/vue')
const aliases = require('./misc/aliases')

environment.loaders.append('vue', vue)
environment.config.merge(aliases)

module.exports = environment
