class MQTTBroker
  require 'mqtt'

  def start(server_number = 1)
    @units = []

    MQTT::Client.connect(
      :host => 'mqtt.flespi.io',
      :port => 8883,
      :ssl => true,
      :username => "FlespiToken cBapqXd6yhGppgxUGbKX4ShQiWyfC7fCwePMZSGlXaAdgVVLXtwP9BvhnRkNOoe8",
      :clean_session => false,
      :client_id => "berry_server_#{server_number}"
    ) do |client|
      @last_time = Time.now.to_i
      self.update_information
      client.get(["$share/recovery/flespi/message/gw/channels/9614/#", 1]) do |topic, message|
        message = JSON.parse(message)

        if (Time.now.to_i - @last_time) > 15.minutes
          @last_time = Time.now.to_i
          self.update_information
        end
        puts message

        # if @units.include?(message['ident'])
        position = {
          latitude: message['position.latitude'],
          longitude: message['position.longitude'],
          speed: message['position.speed'],
          altitude: message['position.altitude'],
          direction: message['position.direction'],
          hdop: message['position.hdop'],
          satellites: message['position.satellites'],
          course: message['position.direction']
        }

        sensors = {}
        message.each do |key, value|
          if key.include?('custom.')
            sensors[key.sub('custom.', '')] = "#{value}|#{value.class}"
          end
        end

        MqttMessage.create({
          ident: message['ident'],
          position: position,
          sensors: sensors,
          mileage: message['vehicle.mileage'],
          date_receive: Time.at(message['timestamp']).utc,
          created_at: Time.at(message['timestamp']).utc,
          updated_at: Time.at(message['timestamp']).utc
        })

        # end
      end
    end
  end

  def update_information
    @units = Array.new

    Backend::MqttUnit.all.each.with_index do |unit, i|
      @units[i] = unit.wialon_imei
    end

  end
end
