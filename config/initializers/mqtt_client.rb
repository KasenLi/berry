class MQTTClient
  require 'mqtt'

  def start(server_number = 1)
    @units = Hash.new
    @units_id = Array.new

    self.set_data

    @last_time = Time.now.to_i

    MQTT::Client.connect(
      :host => 'mqtt.flespi.io',
      :port => 8883,
      :ssl => true,
      :username => "FlespiToken cBapqXd6yhGppgxUGbKX4ShQiWyfC7fCwePMZSGlXaAdgVVLXtwP9BvhnRkNOoe8",
      :clean_session => false,
      :client_id => "berry_server_#{server_number}"
    ) do |mqtt_client|
      mqtt_client.get(["$share/recovery/flespi/message/gw/channels/8066/#", 1]) do |topic, message|
        if (Time.now.to_i - @last_time) > 10.minutes
          @last_time = Time.now.to_i
          self.set_data
        end

        message = JSON.parse(message)
        puts "Mensaje recibido"
        puts message
       
        if (!@units[message['ident'].to_i].nil?)
            
          event = Backend::Event.new
          #event.name = @units[message['ident']][:name]
          event.lat = message['position.latitude']
          event.lon = message['position.longitude']
          event.unit = @units[message['ident'].to_i]
          event.speed = message['custom.speed'].split(' ').first
          event.course = message['custom.course']
          event.address = message['custom.address']
          event.alt = message['position.altitude']
          event.sats = message['custom.satellites']
          event.event_code = message['custom.code']
          event.event_description = message['custom.notification']
          event.date_receive = Time.at(message['timestamp'])

          event.save
          puts message  
        end
      end
    end
  end

  protected
    def set_data
      Backend::MqttUnit.all.each.with_index do |unit, i|
        @units[unit.wialon_id] = unit.wialon_id
      end
    end
end
