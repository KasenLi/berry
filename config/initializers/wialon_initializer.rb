class WialonInitializer
  attr_accessor :wialon

  def initialize(rails_status = false, account)
    if account.wialon_distribution.to_s == 'hosting'
      self.wialon = Wialon.new(@debug=rails_status)
    else
      self.wialon = Wialon.new(@debug=rails_status, @scheme=account.scheme, @host=account.hostname, @port=account.port)
    end

    return self.wialon
  end
end