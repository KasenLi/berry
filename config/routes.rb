Rails.application.routes.draw do
  
  devise_for :monitorists, class_name: "Backend::Monitorist"
  devise_for :operators

  namespace :api, path: '/', constraints: { subdomain: 'api' } do
    root 'application#index'

    get 'integrations/:app', to: 'application#integrations'
  end
  namespace :backend, path: '/' do
    root 'application#index'

    if Rails.env.development?
      mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
    end

    post "/graphql", to: "graphql#execute"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
