class Types::CreditType < Types::BaseObject
  field :id, Integer, null: true
  field :ammount, Float, null: false
  field :billed_ammount, Float, null: false
  field :customer, Types::CustomerType, null: false
  field :billed, Boolean, null: false
end