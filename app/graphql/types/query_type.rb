module Types
  # => Api responses
  class AuthResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :auth, Types::AuthType, null: true
  end

  class WhitelabelsResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :whitelabels, [Types::WhitelabelType], null: true
  end

  class WhitelabelResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :whitelabel, Types::WhitelabelType, null: true
  end

  class ServicesResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :services, [Types::ServiceType], null: true
  end

  class ServiceResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :service, Types::ServiceType, null: true
  end

  class ServiceRecordsResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :service_records, [Types::ServiceRecordType], null: true
  end

  class ServiceRecordResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :service_record, Types::ServiceRecordType, null: true
  end

  class LinkedWialonAccountsResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :linked_wialon_accounts, [Types::LinkedWialonAccountType], null: true
  end

  class UnitsResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :units, [Types::UnitType], null: true
  end

  class UnitResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :unit, Types::UnitType, null: true
  end

  class ServiceTypeResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :service_type, Types::ServiceTypeType, null: true
  end

  class ServiceTypesResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :service_types, [Types::ServiceTypeType], null: true
  end

  class MonitoristResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :monitorist, Types::MonitoristType, null: true
  end

  class MonitoristsResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :monitorists, [Types::MonitoristType], null: true
  end

  class WorkshopResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :workshop, Types::WorkshopType, null: true
  end

  class WorkshopsResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :workshops, [Types::WorkshopType], null: true
  end

  class ParameterResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :parameter, Types::ParameterType, null: true
  end

  class ParametersResponseType < Types::BaseObject
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :parameters, [Types::ParameterType], null: true
  end

  class QueryType < Types::BaseObject
    description "Query Root"

    field :operator, Types::OperatorType, null: false do
      description "Get operator information"
    end

    def operator
      context[:current_operator]
    end

    field :wl, Types::WhitelabelResponseType, null: false do
      description "Get whitelabel by CNAME"
      argument :host, String, required: true
    end

    def wl(host:)
      whitelabel = Whitelabel.find_by({ cname_record: host, approved: true })

      if whitelabel.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, whitelabel: whitelabel }
    end

    field :auth, Types::AuthResponseType, null: false do
      description "Get auth by Token"
      argument :api_token, String, required: true
    end

    def auth(api_token:)
      auth = Operator.find_by({ api_token: api_token })

      if !auth.nil?
        return { status: true, auth: auth }
      end

      auth = User.find_by({ api_token: api_token })

      if !auth.nil?
        return { status: true, auth: auth }
      end

      return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
    end

    field :linked_wialon_accounts, Types::LinkedWialonAccountsResponseType, null: false do
      description "Get all Linked Wialon Acoount by Operator"
      argument :api_token, String, required: true
    end

    def linked_wialon_accounts(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, linked_wialon_accounts: Backend::LinkedWialonAccount.where({ customer_id: operator.customer_id })}
    end

    field :units, Types::UnitsResponseType, null: false do
      description "Get all enabled units by Customer ID"
      argument :api_token, String, required: true
    end
    
    def units(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      return { status: true, units: Backend::MqttUnit.where({ deleted: false, operator_id: operator.id}) }
    end

    field :unit, Types::UnitResponseType, null: false do
      description "Get unit information by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def unit(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      unit = Backend::MqttUnit.find_by({ id: id, operator_id: operator.id, deleted: false })

      if unit.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, unit: unit }
    end

    field :get_units, Types::JsonType, null: true do
      description "Get all enabled units from Monitorist"
      argument :id, Integer, required: false
    end

    def get_units(id:)
      units = Backend::MqttUnit.find_by_sql("SELECT * FROM get_units(#{id.nil? ? 0 : id})")
      
      return { status: true, units: units}
    end

    field :wialon_units, Types::JsonType, null: false do
      description "Get all units from Wialon Account"
      argument :linked_wialon_account_id, Integer, required: true
    end

    def wialon_units(linked_wialon_account_id:)
      account = Backend::LinkedWialonAccount.find(linked_wialon_account_id)

      @wialon = WialonInitializer.new(@debug=(Rails.env == 'development'), account)
      @wialon = @wialon.wialon

      response = @wialon.login(account.token)

      if !response['error'].nil?
        return { status: 403, reason: 'wialon_login' }
      end

      units = @wialon.core_search_items({
        spec: {
          itemsType: "avl_unit",
          propName: "sys_name",
          propValueMask: "*",
          sortType: "sys_name"
        },
        force: 1,
        flags: (1 + 256 + 8388608),
        from: 0,
        to: 0
      })

      @wialon.logout

      if units['error'].nil?
        processed_units = Array.new

        units['items'].each do |unit|
          plate = String.new
          ident = String.new

          if !unit['pflds'].nil?
            unit['pflds'].each do |id, pfld|
              if pfld['n'].downcase == 'registration_plate'
                plate = pfld['v']
                break
              end
            end
          end

          if !unit['uid'].nil?
            ident = unit['uid']
          end

          processed_units.push({
            wialon_id: unit['id'],
            name: unit['nm'], #wialon_name
            wialon_plate: plate,
            ident: ident #wialon_imei
          })
        end

        { status: 200, data: processed_units }
      else
        { status: 403, reason: 'wialon_get_units' }
      end
    end

    field :wialon_accounts, Types::JsonType, null: false do
      description "Get all devices from Wialon Account"
      argument :linked_wialon_account_id, Integer, required: true
    end

    def wialon_accounts(linked_wialon_account_id:)
      account = Backend::LinkedWialonAccount.find(linked_wialon_account_id)

      @wialon = WialonInitializer.new(@debug=(Rails.env == 'development'), account)
      @wialon = @wialon.wialon

      response = @wialon.login(account.token)

      if !response['error'].nil?
        return { status: 403, reason: 'wialon_login' }
      end

      @users = @wialon.core_search_items({
        spec: {
          itemsType: "user",
          propName: "sys_name",
          propValueMask: "*",
          sortType: "sys_name"
        },
        force: 1,
        flags: 1,
        from: 0,
        to: 0
      })

      @wialon.logout

      if !@users['error'].nil?
        return { status: 403, reason: 'get_users' }
      end

      @users['items'].map { |user| { name: user['nm'], id: user['id'] }}
    end

    field :hostname, String, null: false do
      description "Get hostname"
    end

    def hostname
      request.host_with_port
    end

    field :monitorists, Types::MonitoristsResponseType, null: false do
      description "Get all monitorists by Monitorist ID"
      argument :api_token, String, required: true
    end

    def monitorists(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, monitorists: Backend::Monitorist.where({ deleted: false, operator_id: operator.id })}
    end

    field :monitorist, Types::MonitoristResponseType, null: true do
      description "Get monitorist by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def monitorist(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      monitorist = Backend::Monitorist.find_by({ id: id, operator_id: operator.id, deleted: false })

      if monitorist.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, monitorist: monitorist }
    end

    field :services, Types::ServicesResponseType, null: false do
      description "Get all services by Token"
      argument :api_token, String, required: true
    end

    def services(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, services: Backend::Service.where({ deleted: false, operator_id: operator.id })}
    end

    field :service, Types::ServiceResponseType, null: true do
      description "Get service by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def service(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      service = Backend::Service.find_by({ id: id, operator_id: operator.id, deleted: false })

      if service.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, service: service }
    end

    field :service_records, Types::ServiceRecordsResponseType, null: false do
      description "Get all serviceRecords by Customer ID"
      argument :api_token, String, required: true
    end

    def service_records(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, service_records: Backend::ServiceRecord.where({ deleted: false, operator_id: operator.id })}
    end

    field :service_record, Types::ServiceRecordResponseType, null: true do
      description "Get serviceRecord by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def service_record(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      serviceRecord = Backend::ServiceRecord.find_by({ id: id, operator_id: operator.id, deleted: false })

      if serviceRecord.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, service_record: serviceRecord }
    end

    field :service_types, Types::ServiceTypesResponseType, null: false do
      description "Get all services by Service ID"
      argument :api_token, String, required: true
    end

    def service_types(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, service_types: Backend::ServiceType.where({ deleted: false, operator_id: operator.id })}
    end

    field :service_type, Types::ServiceTypeResponseType, null: true do
      description "Get service by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def service_type(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      serviceType = Backend::ServiceType.find_by({ id: id, operator_id: operator.id, deleted: false })

      if serviceType.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, service_type: serviceType }
    end

    field :workshops, Types::WorkshopsResponseType, null: false do
      description "Get all workshops by customer ID"
      argument :api_token, String, required: true
    end

    def workshops(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, workshops: Backend::Workshop.where({ deleted: false, operator_id: operator.id })}
    end

    field :workshop, Types::WorkshopResponseType, null: true do
      description "Get workshop by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def workshop(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      workshop = Backend::Workshop.find_by({ id: id, operator_id: operator.id, deleted: false })

      if workshop.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, workshop: workshop }
    end

    field :parameters, Types::ParametersResponseType, null: false do
      description "Get all parameters by customer ID"
      argument :api_token, String, required: true
    end

    def parameters(api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      return { status: true, parameters: Backend::Parameter.where({ deleted: false, operator_id: operator.id })}
    end

    field :parameter, Types::ParameterResponseType, null: true do
      description "Get parameter by ID"
      argument :id, Integer, required: true
      argument :api_token, String, required: true
    end

    def parameter(id:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      parameter = Backend::Parameter.find_by({ id: id, operator_id: operator.id, deleted: false })

      if parameter.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      return { status: true, parameter: parameter }
    end
  end
end
