class Types::OperatorType < Types::BaseObject
  field :id, Integer, null: true
  field :name, String, null: false
  field :last_name, String, null: false
  field :username, String, null: false
  field :api_token, String, null: false
  field :email, String, null: false
  field :sidebar_status, Boolean, null: false
  field :rates, Types::RateType, null: false
  field :customer_id, Integer, null: false
  field :parsed_berry_permissions, Types::JsonType, null: false
  field :form_berry_permissions, Types::JsonType, null: false
  field :permissions_processed, Types::JsonType, null: false
end