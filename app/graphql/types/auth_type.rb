module Types
	class AuthType < Types::BaseUnion
		description "Auth"
		possible_types Types::OperatorType, Types::UserType

		def self.resolve_type(object, context)
			if object.is_a?(User)
				Types::UserType
			else
				Types::OperatorType
			end
		end
	end
end