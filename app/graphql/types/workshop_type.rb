class Types::WorkshopType < Types::BaseObject
  field :id, Integer, null: true
  field :name, String, null: false
  field :service, String, null: false
  field :state, Integer, null: false
  field :backend_mqtt_unit_id, Integer, null: false
  field :date_in, String, null: false
  field :date_out, String, null: true

  field :unit, Types::UnitType, null: false
  field :status, String, null: false
  field :unit_name, String, null: false

  field :operator_id, Integer, null: false
  field :customer_id, Integer, null: false

  def unit
    unit = Backend::MqttUnit.find(self.backend_mqtt_unit_id)

    unit
  end

  def unit_name
    unit = Backend::MqttUnit.find(self.backend_mqtt_unit_id)

    unit_name = unit.wialon_name

    unit_name
  end
end