class Types::ServiceTypeType < Types::BaseObject
  field :id, Integer, null: true
  field :name, String, null: false

  field :operator_id, Integer, null: false
  field :customer_id, Integer, null: false

end