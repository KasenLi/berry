module Types
  class MutationType < Types::BaseObject
    field :updateSidebar, mutation: Mutations::UpdateSidebar

    field :authenticate, mutation: Mutations::Authenticate

    field :updateActivated, mutation: Mutations::UpdateActivated

    # => Units
    field :importUnits, mutation: Mutations::ImportUnits

    # => Monitorists
    field :addMonitorist, mutation: Mutations::AddMonitorist
    field :editMonitorist, mutation: Mutations::EditMonitorist
    field :deleteMonitorist, mutation: Mutations::DeleteMonitorist

    # => Services
    field :addService, mutation: Mutations::AddService
    field :editService, mutation: Mutations::EditService
    field :deleteService, mutation: Mutations::DeleteService

    # => Service records
    field :addServiceRecord, mutation: Mutations::AddServiceRecord
    field :editServiceRecord, mutation: Mutations::EditServiceRecord
    field :deleteServiceRecord, mutation: Mutations::DeleteServiceRecord

    # => Workshops
    field :addWorkshop, mutation: Mutations::AddWorkshop
    field :editWorkshop, mutation: Mutations::EditWorkshop
    field :deleteWorkshop, mutation: Mutations::DeleteWorkshop

    # => Parameters
    field :addParameter, mutation: Mutations::AddParameter
    field :editParameter, mutation: Mutations::EditParameter
    field :deleteParameter, mutation: Mutations::DeleteParameter
  end
end
