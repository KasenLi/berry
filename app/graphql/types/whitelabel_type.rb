module Types
  class WhitelabelType < Types::BaseObject
    field :id, Integer, null: true
    field :brand, String, null: false
    field :cname_record, String, null: false
    field :app, String, null: false
    field :color, String, null: false
    field :logo, String, null: false
    field :page_title, String, null: false
    field :footer_url, String, null: false
    field :footer_name, String, null: false
    field :approved, Boolean, null: false
    field :customer_id, Integer, null: false
    
    field :customer_id, Integer, null: false
  end
end
