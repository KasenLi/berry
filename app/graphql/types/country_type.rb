class Types::CountryType < Types::BaseObject
  field :id, Integer, null: false
  field :code, String, null: true
  field :common, String, null: true
  field :official, String, null: true
  field :flag, String, null: true
end