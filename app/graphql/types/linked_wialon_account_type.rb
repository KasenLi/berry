class Types::LinkedWialonAccountType < Types::BaseObject
  field :id, Integer, null: true
  field :username, String, null: true
  field :wialonDistribution, String, null: false
  field :hostname, String, null: true
  field :scheme, String, null: true
  field :port, Integer, null: false
  field :token, String, null: true
  field :default_account, Boolean, null: false
  field :customer_id, Integer, null: false
  
  field :distribution, String, null: false

  def distribution
    I18n.t("activerecord.attributes.backend/linked_wialon_account.#{self.wialon_distribution}")
  end
end