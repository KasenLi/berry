class Types::ServiceType < Types::BaseObject
  field :id, Integer, null: true
  field :name, String, null: false
  field :frequency_km, Integer, null: false
  field :frequency_time, Integer, null: false
  field :time_type, Integer, null: false
  field :frequency_engine, Integer, null: false
  field :units, [Types::UnitType], null: false
  field :activated, Boolean, null: false
  
  field :frequency, String, null: false
  field :units_count, Integer, null: false
  field :frequency_type, Types::JsonType, null: false

  field :operator_id, Integer, null: false
  field :customer_id, Integer, null: false

  def frequency
    if self.time_type == 1
      type = " Horas"
    else
      type = " Días"
    end
    if self.frequency_km > 0 && self.frequency_time == 0 && self.frequency_engine == 0
      frequency = self.frequency_km.to_s + " Km"
    elsif self.frequency_time > 0 && self.frequency_km == 0 && self.frequency_engine == 0
      frequency = self.frequency_time.to_s + type
    elsif self.frequency_engine > 0 && self.frequency_km == 0 && self.frequency_time == 0
      frequency = self.frequency_engine.to_s + " Horas de motor"
    elsif self.frequency_km > 0 && self.frequency_time > 0 && self.frequency_engine > 0
      frequency = self.frequency_km.to_s + " Km" + " / " + self.frequency_time.to_s + type + " / " + self.frequency_engine.to_s + " Horas de motor"
    elsif self.frequency_km > 0 && self.frequency_time > 0
      frequency = self.frequency_km.to_s + " Km / " + self.frequency_time.to_s + type 
    elsif self.frequency_km > 0 && self.frequency_engine > 0
      frequency = self.frequency_km.to_s + " Km" + " / " + self.frequency_engine.to_s + " Horas de motor"
    elsif self.frequency_time > 0 && self.frequency_engine > 0
      frequency = self.frequency_time.to_s + type + " / " + self.frequency_engine.to_s + " Horas de motor"
    else
      frequency = "Sin tiempo"
    end

    frequency
  end

  def units_count
    units_count = self.units.count

    units_count
  end

  def frequency_type
    if self.frequency_km > 0
      frequency_km = true
    else
      frequency_km = false
    end
    if self.frequency_time > 0
      frequency_time = true
    else 
      frequency_time = false
    end
    if self.frequency_engine > 0
      frequency_engine = true
    else 
      frequency_engine = false
    end

    frequency_type = {frequency_km: frequency_km, frequency_time: frequency_time, frequency_engine: frequency_engine}
    
    frequency_type    
  end
end