module Types
  class UserType < Types::BaseObject
    field :id, Integer, null: false
    field :email, String, null: false
    field :name, String, null: false
    field :api_token, String, null: false
    field :permissions, Types::JsonType, null: false
    field :formPermissions, Types::JsonType, null: false
    field :parsedPermissions, Types::JsonType, null: false
  end
end
