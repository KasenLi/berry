class Types::UnitType < Types::BaseObject
  field :id, Integer, null: true
  
  field :wialon_name, String, null: false
  field :wialon_imei, String, null: false
  field :wialon_plate, String, null: false
  field :wialon_economic, String, null: true
  field :wialon_vin, String, null: true
  field :wialon_id, Integer, null: false
  field :linked_wialon_account_id, Integer, null: true
  field :operator_id, Integer, null: true
  field :customer_id, Integer, null: true

  field :deleted, Boolean, null: true

end