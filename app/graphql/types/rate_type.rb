class Types::RateType < Types::BaseObject
  field :id, Integer, null: false
  field :rates, Types::JsonType, null: false
  field :limits, Types::JsonType, null: true
  field :start_at, String, null: false
  field :customer_id, Integer, null: false
end