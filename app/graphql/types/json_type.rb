class Types::JsonType < Types::BaseScalar
  def coerce_input(x)
    JSON.parse(x)
  end

  def coerce_result(x)
    JSON.dump(x)
  end
end