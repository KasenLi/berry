class Types::TransactionType < Types::BaseObject
  field :id, Integer, null: false
  field :type_consumption, String, null: false
  field :quantity_consumption, Integer, null: false
  field :date_at, String, null: false
  field :billed, Boolean, null: false
  field :customer_id, Integer, null: false
end
