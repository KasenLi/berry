class Types::MonitoristType < Types::BaseObject
  field :id, Integer, null: true
  field :username, String, null: false
  field :name, String, null: false
  field :email, String, null: false
  field :phone, String, null: false
  field :units, [Types::UnitType], null: false

  field :operator_id, Integer, null: false
  field :customer_id, Integer, null: false

end