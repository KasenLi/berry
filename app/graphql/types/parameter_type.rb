class Types::ParameterType < Types::BaseObject
  field :id, Integer, null: true
  field :name, String, null: false
  field :es_name, String, null: false
  field :chart_type, Integer, null: false
  field :measure, String, null: false
  field :modal, Boolean, null: false
  field :modal_type, Integer, null: false
  field :extra_chart, Boolean, null: false
  field :extra_chart_type, String, null: false
  field :lim_legends, Types::JsonType, null: false
  field :icon, String, null: false

  field :operator_id, Integer, null: false
  field :customer_id, Integer, null: false

end