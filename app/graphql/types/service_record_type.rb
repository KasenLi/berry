class Types::ServiceRecordType < Types::BaseObject
  field :id, Integer, null: true
  field :name, String, null: false
  field :state, Integer, null: false
  field :unit_id, Integer, null: false
  field :date, String, null: false

  field :unit, Types::UnitType, null: false
  field :status, String, null: false
  field :unit_name, String, null: false

  field :operator_id, Integer, null: false
  field :customer_id, Integer, null: false
  
  field :actions, Types::JsonType, null: false

  def unit
    unit = Backend::MqttUnit.find(self.unit_id)

    unit
  end

  def unit_name
    unit = Backend::MqttUnit.find(self.unit_id)

    unit_name = unit.wialon_name

    unit_name
  end

  def status
    if self.state == 0 
      status = "A realizar"
    elsif self.state == 1
      status = "En progreso"
    else 
      status = "Indefinido"
    end
    status
  end
end