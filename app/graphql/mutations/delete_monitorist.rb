module Mutations
  class DeleteMonitorist < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :monitorist, [Types::MonitoristType], null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :id, Integer, required: true

    def resolve(**args)

      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      monitorist = Backend::Monitorist.find_by({ id: args[:id], operator_id: operator.id })

      if monitorist.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      if monitorist.update({ deleted: true, deleted_at: Time.now })
        monitorist.units = []
        return { status: true, monitorist: Backend::Monitorist.where({ deleted: false, operator_id: operator.id }) }
      else
        return { status: false, errors: monitorist.errors.messages }
      end
      # permissions = context[:current_operator].parsed_berry_permissions['monitorist']
      
      # if permissions['delete']
      #   monitorist = Backend::Monitorist.find(id)
      #   state = monitorist.update({ deleted: true, deleted_at: Time.now.utc })
      # else
      #   state = false
      # end

      # return { status: state, monitorist: Backend::Monitorist.where(deleted: false, customer_id: context[:current_operator].customer_id) }
    end
  end
end
