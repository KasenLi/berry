module Mutations
  class AddMonitorist < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :monitorist, Types::MonitoristType, null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :username, String, required: true
    argument :name, String, required: true
    argument :email, String, required: true
    argument :phone, String, required: true
    argument :units, [Integer], required: false

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])
      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      @monitorist = Backend::Monitorist.new
      @monitorist.username = args[:username]
      @monitorist.email = args[:email]
      @monitorist.name = args[:name]
      @monitorist.phone = args[:phone]
      password = SecureRandom.base64(15)
      @monitorist.password = password

      @monitorist.operator_id = operator.id
      @monitorist.customer_id = operator.customer_id

      if @monitorist.save
        MonitoristMailer.send_account(password, @monitorist).deliver_now
        if args[:units]
          set_units(args[:units])
        end
        return { status: true, monitorist: @monitorist }
      else
        return { status: false, errors: @monitorist.errors.full_messages, monitorist: @monitorist }
      end
    end

    def set_units(units)
      flag = []

      units.each do |id|
        flag2 = Backend::MqttUnit.find(id.to_i)
        if flag2.nil?
          next
        end
        flag[flag.length] = flag2
      end

      @monitorist.units = flag
    end
  end
end
