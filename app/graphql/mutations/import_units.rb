module Mutations
  class ImportUnits < GraphQL::Schema::RelayClassicMutation
    include TransactionsHelper

    # => Fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :units, [Types::UnitType], null: true

    # => Arguments
    argument :linked_wialon_account_id, Integer, required: true
    argument :units, String, required: true
    argument :api_token, String, required: true

    # => Resolve
    def resolve(linked_wialon_account_id:, units:, api_token:)
      operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      if !operator.form_berry_permissions['units.add']
        return { status: false, errors: [I18n.t('permissions.unauthorized')]}
      end

      rates = operator.rates.rates
      rate = rates["gcc.unit"].to_f

      created = Array.new
      errors = Array.new

      JSON.parse(units).each do |unit|
        unit = unit.symbolize_keys
        db_unit = Backend::MqttUnit.find_or_initialize_by({ wialon_id: unit[:wialon_id]})

        if db_unit.id.nil?
          db_unit.customer_id = operator.customer_id
          db_unit.operator_id = operator.id
        end
        db_unit.wialon_name = unit[:wialon_name]
        db_unit.wialon_imei = unit[:wialon_imei]
        db_unit.wialon_plate = unit[:wialon_plate]
        db_unit.linked_wialon_account_id = linked_wialon_account_id

        if db_unit.id.nil?
          ammount = calculate_prorate(rate, 1)

          transaction = Backend::Consumption.new
          transaction.type_consumption = "gcc.unit"
          transaction.quantity_consumption = 1
          transaction.ammount = ammount
          transaction.date_at = Time.now
          transaction.customer_id = operator.customer_id
          transaction.save
        end

        if db_unit.save
          created.push(db_unit)
        else
          db_unit.errors.full_messages.each do |error|
            errors.push(error)
          end
        end
      end

      return { status: true, errors: errors, units: created }
    end
  end
end
