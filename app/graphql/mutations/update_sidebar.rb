module Mutations
  class UpdateSidebar < GraphQL::Schema::RelayClassicMutation
    field :sidebar_status, Boolean, null: false

    argument :sidebar_status, Boolean, required: true

    def resolve(sidebar_status:)
      context[:current_operator].update({ sidebar_status: sidebar_status })
      return { sidebar_status: context[:current_operator].sidebar_status }
    end
  end
end
