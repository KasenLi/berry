module Mutations
  class Authenticate < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, [String], null: true
    field :auth, Types::AuthType, null: true

    # => Arguments (Input fields)
    argument :username, String, required: true
    argument :password, String, required: true

    def resolve(**args)
      auth = Operator.where("username = :username OR email = :username", { username: args[:username] }).first

      if !auth.nil?
        if auth.valid_password?(args[:password])
          self.sign_in(auth)
          return { status: true, auth: auth }
        else
          return { status: false, errors: [I18n.t('devise.failure.invalid')] }
        end
      end

      auth = User.where("username = :username OR email = :username", { username: args[:username] }).first

      if !auth.nil?
        if auth.valid_password?(args[:password])
          if self.validate_access(auth.permissions)
            self.sign_in(auth)
            return { status: true, auth: auth }
          else
            return { status: false, errors: [I18n.t('errors.access_denied')] }
          end
        else
          return { status: false, errors: [I18n.t('devise.failure.invalid')] }
        end
      end

      return { status: false, errors: [I18n.t('devise.failure.invalid')] }
    end

    def sign_in(object)
      object.update({
        current_sign_in_at: Time.now,
        sign_in_count: object.sign_in_count + 1
      })
    end

    def validate_access(permissions)
      flag = false

      permissions.each do |key, value|
        if key.include?('berry') && !key.include?('loginAs')
          if eval(value)
            flag = true
          end
        end
      end

      return flag
    end
  end
end
