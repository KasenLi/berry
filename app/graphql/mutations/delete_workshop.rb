module Mutations
  class DeleteWorkshop < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :workshop, [Types::WorkshopType], null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :id, Integer, required: true

    def resolve(**args)

      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      workshop = Backend::Workshop.find_by({ id: args[:id], customer_id: operator.customer_id })

      if workshop.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      if workshop.update({ deleted: true, deleted_at: Time.now })
        return { status: true, workshops: Backend::Workshop.where({ deleted: false, customer_id: operator.customer_id }) }
      else
        return { status: false, errors: workshop.errors.messages }
      end
      # permissions = context[:current_operator].parsed_berry_permissions['serviceRecords']
      
      # if permissions['delete']
      #   serviceRecord = Backend::ServiceRecord.find(id)
      #   state = serviceRecord.update({ deleted: true, deleted_at: Time.now.utc })
      # else
      #   state = false
      # end

      # return { status: state, serviceRecords: Backend::ServiceRecord.where(deleted: false, customer_id: context[:current_operator].customer_id) }
    end
  end
end
