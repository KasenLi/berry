module Mutations
  class AddServiceRecord < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :service_record, Types::ServiceRecordType, null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :name, String, required: true
    argument :unit_id, Integer, required: true
    argument :date, String, required: true
    argument :state, Integer, required: true
    argument :is_save, Boolean, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      
      serviceRecord = Backend::ServiceRecord.new
      serviceRecord.name = args[:name]
      serviceRecord.unit_id = args[:unit_id]
      serviceRecord.state = args[:state]
      serviceRecord.date = args[:date]

      serviceRecord.operator_id = operator.id
      serviceRecord.customer_id = operator.customer_id

      if serviceRecord.save
        if args[:is_save]

          service_type = Backend::ServiceType.new
          service_type.name = args[:name]

          service_type.operator_id = operator.id
          service_type.customer_id = operator.customer_id

          service_type.save
        end
        return { status: true, service_record: serviceRecord }
      else
        return { status: false, errors: serviceRecord.errors.full_messages, service_record: serviceRecord }
      end
    end
  end
end
