module Mutations
  class AddParameter < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :parameter, Types::ParameterType, null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :name, String, required: true
    argument :es_name, String, required: true
    argument :type, String, required: true
    argument :measure, String, required: true
    argument :modal, Boolean, required: false
    argument :modal_type, String, required: true
    argument :extra_chart, Boolean, required: true
    argument :extra_chart_type, String, required: false
    argument :lim_legends, Types::JsonType, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      
      parameter = Backend::Parameter.new
      parameter.name = args[:name]
      parameter.es_name = args[:es_name]
      parameter.type = args[:type]
      parameter.measure = args[:measure]
      parameter.modal = args[:modal]
      parameter.modal_type = args[:modal_type]
      parameter.extra_chart = args[:extra_chart]
      parameter.extra_chart_type = args[:extra_chart_type]
      parameter.lim_legends = args[:lim_legends]

      parameter.operator_id = operator.id
      parameter.customer_id = operator.customer_id

      if parameter.save
        return { status: true, parameter: parameter }
      else
        return { status: false, errors: parameter.errors.full_messages, parameter: parameter }
      end
    end
  end
end
