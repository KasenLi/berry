module Mutations
  class AddWorkshop < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :workshop, Types::WorkshopType, null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :name, String, required: true
    argument :service, String, required: true
    argument :unit_id, Integer, required: true
    argument :date_in, String, required: true
    argument :date_out, String, required: false
    argument :state, Integer, required: true
    argument :is_save, Boolean, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      
      workshop = Backend::Workshop.new
      workshop.name = args[:name]
      workshop.backend_mqtt_unit_id = args[:unit_id]
      workshop.state = args[:state]
      workshop.date_in = args[:date_in]

      if args[:date_out]
        workshop.date_out = args[:date_out]
      end
      
      workshop.service = args[:service]

      workshop.operator_id = operator.id
      workshop.customer_id = operator.customer_id

      if workshop.save
        if args[:is_save]

          service_type = Backend::ServiceType.new
          service_type.name = args[:service]

          service_type.operator_id = operator.id
          service_type.customer_id = operator.customer_id

          service_type.save
        end
        return { status: true, workshop: workshop }
      else
        return { status: false, errors: workshop.errors.full_messages, workshop: workshop }
      end
    end
  end
end
