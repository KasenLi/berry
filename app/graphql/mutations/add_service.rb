module Mutations
  class AddService < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :service, Types::ServiceType, null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :name, String, required: true
    argument :units, [Integer], required: false
    argument :frequency_km, Integer, required: false
    argument :frequency_time, Integer, required: false
    argument :time_type, Integer, required: false
    argument :frequency_engine, Integer, required: false
    argument :activated, Boolean, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      @service = Backend::Service.new
      @service.name = args[:name]
      
      if args[:frequency_km]
        @service.frequency_km = args[:frequency_km]
      end

      if args[:frequency_time]
        @service.frequency_time = args[:frequency_time]
        @service.time_type = args[:time_type]
      end

      if args[:frequency_engine]
        @service.frequency_engine = args[:frequency_engine]
      end
      
      @service.operator_id = operator.id
      @service.customer_id = operator.customer_id

      @service.activated = args[:activated]
      @service.activated_at = Time.now

      if @service.save
        if args[:units]
          set_units(args[:units])
        end
        return { status: true, service: @service }
      else
        return { status: false, errors: @service.errors.full_messages, service: @service }
      end
    end

    def set_units(units)
      flag = []
      
      mileage = 0.0
      engine_hours = 0.0
      
      units.each do |id|
        flag2 = Backend::MqttUnit.find(id.to_i)
        if flag2.nil?
          next
        end
        flag[flag.length] = flag2
        
      end

      @service.units = flag
      units.each do |id|
        message = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                      WHERE ident = '868789020849468' AND mileage > 0
                                                      ORDER BY date_receive DESC LIMIT 1;")
        message2 = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                      WHERE ident = '862894022409166' AND mileage > 0
                                                      ORDER BY date_receive DESC LIMIT 1;")

        mileage = message2[0]['mileage'].to_f
        if message[0]['sensors']['hour_meter_count'].nil?
          engine_hours = 0
        else
          engine_hours = message[0]['sensors']['hour_meter_count'].to_f
        end
        
        set_mileage(id, mileage, engine_hours)
      end
      
    end

    def set_mileage(unit_id, mileage, engine_hours)
      @relation = Backend::MqttUnitsService.find_by(mqtt_unit_id: unit_id.to_i, service_id: @service.id)
      @relation.mileage = mileage.to_f
      @relation.engine_hours = engine_hours.to_f
      @relation.save
    end
  end
end
