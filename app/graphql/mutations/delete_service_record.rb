module Mutations
  class DeleteServiceRecord < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :service_records, [Types::ServiceRecordType], null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :id, Integer, required: true

    def resolve(**args)

      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      serviceRecord = Backend::ServiceRecord.find_by({ id: args[:id], customer_id: operator.customer_id })

      if serviceRecord.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      if serviceRecord.update({ deleted: true, deleted_at: Time.now })
        return { status: true, service_records: Backend::ServiceRecord.where({ deleted: false, customer_id: operator.customer_id }) }
      else
        return { status: false, errors: serviceRecord.errors.messages }
      end
      # permissions = context[:current_operator].parsed_berry_permissions['serviceRecords']
      
      # if permissions['delete']
      #   serviceRecord = Backend::ServiceRecord.find(id)
      #   state = serviceRecord.update({ deleted: true, deleted_at: Time.now.utc })
      # else
      #   state = false
      # end

      # return { status: state, serviceRecords: Backend::ServiceRecord.where(deleted: false, customer_id: context[:current_operator].customer_id) }
    end
  end
end
