module Mutations
  class EditWorkshop < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :workshop, Types::WorkshopType, null: false

    # => Arguments
    argument :id, Integer, required: true
    argument :api_token, String, required: true
    argument :name, String, required: true
    argument :service, String, required: true
    argument :unit_id, Integer, required: true
    argument :date_in, String, required: true
    argument :date_out, String, required: false
    argument :state, Integer, required: true
    argument :is_save, Boolean, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      workshop = Backend::Workshop.find_by({ id: args[:id], customer_id: operator.customer_id})
      
      if workshop.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      data = {
        name: args[:name],
        service: args[:service],
        backend_mqtt_unit_id: args[:unit_id],
        state: args[:state],
        date_in: args[:date_in],
        date_out: args[:date_out]
      }

      if workshop.update(data)
        if args[:is_save]

          service_type = Backend::ServiceType.new
          service_type.name = args[:service]

          service_type.operator_id = operator.id
          service_type.customer_id = operator.customer_id

          service_type.save
        end
        return { status: true, workshop: workshop }
      else
        return { status: false, errors: convert_to_camel_case(workshop.errors.messages), workshop: workshop }
      end
    end
  end
end
