module Mutations
  class EditMonitorist < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :monitorist, Types::MonitoristType, null: false

    # => Arguments
    argument :id, Integer, required: true
    argument :api_token, String, required: true
    argument :username, String, required: true
    argument :name, String, required: true
    argument :email, String, required: true
    argument :phone, String, required: true
    argument :units, [Integer], required: false

    def resolve(**args)

      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      @monitorist = Backend::Monitorist.find_by({ id: args[:id], customer_id: operator.customer_id})

      if @monitorist.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      data = {
        email: args[:email],
        name: args[:name],
        phone: args[:phone],
        username: args[:username]
      }

      if @monitorist.update(data)
        if args[:units]
          set_units(args[:units])
        end
        return { status: true, monitorist: @monitorist }
      else
        return { status: false, errors: convert_to_camel_case(monitorist.errors.messages), monitorist: @monitorist }
      end
    end

    def set_units(units)
      flag = []

      units.each do |id|
        flag2 = Backend::MqttUnit.find(id.to_i)
        if flag2.nil?
          next
        end
        flag[flag.length] = flag2
      end

      @monitorist.units = flag
    end
  end
end
