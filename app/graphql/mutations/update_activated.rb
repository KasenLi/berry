module Mutations
	class UpdateActivated < GraphQL::Schema::RelayClassicMutation
		# => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :service, Types::ServiceType, null: false

    argument :activated, Boolean, required: true
    argument :id, Integer, required: true
    argument :api_token, String, required: true

    def resolve(id:, activated:, api_token:)
    	operator = Operator.find_by(api_token: api_token)

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
    	service = Backend::Service.find_by(id: id, customer_id: operator.customer_id)

    	if activated == false
    		if service.update({activated: !activated, activated_at: Time.now})
	    		return { status: true, service: service}
	    	else
	    		return { status: false, errors: convert_to_camel_case(service.errors.messages), service: service }
	    	end
	    else
	    	if service.update({activated: !activated})
	    		return { status: true, service: service}
	    	else
	    		return { status: false, errors: convert_to_camel_case(service.errors.messages), service: service }
	    	end
    	end
    end	
	end

end