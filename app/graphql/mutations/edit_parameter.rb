module Mutations
  class EditParameter < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :parameter, Types::ParameterType, null: false

    # => Arguments
    argument :id, Integer, required: true
    argument :api_token, String, required: true
    argument :name, String, required: true
    argument :es_name, String, required: true
    argument :type, String, required: true
    argument :measure, String, required: true
    argument :modal, Boolean, required: false
    argument :modal_type, String, required: true
    argument :extra_chart, Boolean, required: true
    argument :extra_chart_type, String, required: false
    argument :lim_legends, Types::JsonType, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end
      parameter = Backend::Parameter.find_by({ id: args[:id], customer_id: operator.customer_id})
      
      if parameter.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      data = {
        name: args[:name],
        es_name: args[:es_name],
        type: args[:type],
        measure: args[:measure],
        modal: args[:modal],
        modal_type: args[:modal_type],
        extra_chart: args[:extra_chart],
        extra_chart_type: args[:extra_chart_type],
        lim_legends: args[:lim_legends]
      }

      if parameter.update(data)
        return { status: true, parameter: parameter }
      else
        return { status: false, errors: convert_to_camel_case(parameter.errors.messages), parameter: parameter }
      end
    end
  end
end
