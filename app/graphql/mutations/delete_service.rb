module Mutations
  class DeleteService < GraphQL::Schema::RelayClassicMutation
    # => Return fields
    field :status, Boolean, null: false
    field :errors, Types::JsonType, null: true
    field :services, [Types::ServiceType], null: false

    # => Arguments
    argument :api_token, String, required: true
    argument :id, Integer, required: true

    def resolve(**args)
      operator = Operator.find_by(api_token: args[:api_token])

      if operator.nil?
        return { status: false, errors: [I18n.t('devise.failure.invalid_token')] }
      end

      service = Backend::Service.find_by({ id: args[:id], customer_id: operator.customer_id })

      if service.nil?
        return { status: false, errors: [I18n.t('errors.invalid_id')] }
      end

      if service.update({ deleted: true, deleted_at: Time.now })
        service.units = []
        return { status: true, services: Backend::Service.where({ deleted: false, operator_id: operator.id }) }
      else
        return { status: false, errors: service.errors.messages }
      end
      # permissions = context[:current_operator].parsed_berry_permissions['services']
      
      # if permissions['delete']
      #   service = Backend::Service.find(id)
      #   state = service.update({ deleted: true, deleted_at: Time.now.utc })
      # else
      #   state = false
      # end

      # return { status: state, services: Backend::Service.where(deleted: false, customer_id: context[:current_operator].customer_id) }
    end
  end
end
