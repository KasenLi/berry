class Customer < ViggoRecord
  include TransactionsHelper

  # => Devise fields
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :confirmable,
         :lockable

  # => Validations
  validates :email, presence: true, uniqueness: true
  validates :company, presence: true, length: 2..255
  validates :name, presence: true, length: 2..255
  validates :phone, presence: true, length: 2..255

  # => Relationships
  belongs_to :country, class_name: 'Backend::Country', foreign_key: 'backend_country_id'

  # => Enum
  enum metric_system: [:international_system, :english_system]

  before_create :set_access_token
    
  def rates
    Backend::Rate.find_by(customer_id: self.id)
  end

  def credits
    Backend::Credit.where(customer_id: self.id).order(id: :ASC)
  end

  def linked_wialon_accounts
    Backend::LinkedWialonAccount.where(customer_id: self.id).order(id: :ASC)
  end

  def operators
    Operator.where(customer_id: self.id).order(id: :ASC)
  end

  def available_credits
    credits = 0.0
    Backend::Credit.where(customer_id: self.id, billed: false).each do |credit|
      credits += credit.ammount
      credits -= credit.billed_ammount
    end

    credits
  end

  def transactions
    Backend::Consumption.where(customer_id: self.id, billed: true).order(id: :ASC)
  end

  def consumpted_credits
    ammount = 0.0
    time = Time.zone.now.all_month
    rates = self.rates.rates
    Backend::Consumption.where(customer_id: self.id, billed: true).where("created_at BETWEEN ? AND ?", time.first, time.last).each do |transaction|
      rate = (rates[transaction.type_consumption.to_s]).to_f

      ammount += calculate_prorate(rate, transaction.quantity_consumption, transaction.date_at.day)
    end

    ammount
  end

  def parent
    if self.parent_id == 0
      self
    else
      Customer.find(self.parent_id)
    end
  end

  private
    def set_access_token
      while true
        flag = Generator.new.generate_token
        if Customer.where(api_token: flag).empty?
          self.api_token = flag
          break
        end
      end
    end
end
