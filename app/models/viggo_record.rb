class ViggoRecord < ActiveRecord::Base
  establish_connection("cms_berry_#{Rails.env}".to_sym)
  self.abstract_class = true
  self.table_name_prefix = ''
end
