class Backend::MqttUnit < ApplicationRecord
	has_and_belongs_to_many :services, classname: 'Backend::Service'

	has_and_belongs_to_many :monitorists, classname: 'Backend::Monitorist'

	belongs_to :workshop, class_name: "Backend::Workshop", foreign_key: "backend_mqtt_unit_id"
end
