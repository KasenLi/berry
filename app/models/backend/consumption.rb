class Backend::Consumption < ViggoRecord
  enum type_consumption: [
    :'instora.installer',
    :'instora.operator',
    :'gcc.unit',
    :'gcc.operator',
    :'mfence.multiaccount',
    :'mfence.mobile',
    :'signaly.transport',
    :'signaly.unit',
    :'alpha.protocol.launch',
    :'alpha.protocol.review',
    :'alpha.unit',
    :'gosapp.message',
    :'gosapp.channel',
  ]

  def customer
    Customer.find(self.customer_id)
  end
end
