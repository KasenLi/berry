class Backend::Service < ApplicationRecord
	has_and_belongs_to_many :units, class_name: 'Backend::MqttUnit'

	validates :name, presence: true, length: 2..120
end
