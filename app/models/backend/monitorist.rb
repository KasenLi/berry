class Backend::Monitorist < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, presence: true, length: 2..255
  validates :phone, presence: true, length: 2..64
  
  has_and_belongs_to_many :units, class_name: "Backend::MqttUnit"
end
