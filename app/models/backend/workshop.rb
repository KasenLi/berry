class Backend::Workshop < ApplicationRecord
	has_one :unit, class_name: "Backend::MqttUnit", foreign_key: 'backend_mqtt_unit_id'

	validates :service, presence: true, length: 2..120
	validates :name, presence: true, length: 2..120
	validates :state, presence: true, inclusion: { in: 0..2 }
	validates :backend_mqtt_unit_id, presence: true
	validates :date_in, presence: true
end
