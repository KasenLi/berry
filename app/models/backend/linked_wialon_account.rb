class Backend::LinkedWialonAccount < ViggoRecord
  # => Validations
  validates :username, presence: true, length: 2..120
  validates :token, presence: true, uniqueness: true
  validates :port, presence: true
  validates :scheme, presence: true, inclusion: { in: ['http', 'https'] }
  validates :hostname, presence: true
  validates :wialon_distribution, presence: true
  validates :default_account, presence: true, if: :unique_default_account?

  # => Enums
  enum wialon_distribution: ['hosting', 'local']

  def unique_default_account?
    flag = Backend::LinkedWialonAccount.where(customer_id: self.customer_id, default_account: true)
    if flag.count > 0 && flag.id != self.id
      errors.add(default_account: :uniqueness)
    end
  end

  def customer
    Customer.find(self.customer_id)
  end
end
