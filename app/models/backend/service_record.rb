class Backend::ServiceRecord < ApplicationRecord

	validates :name, presence: true, length: 2..120
	validates :date, presence: true
	validates :state, presence: true, inclusion: { in: 0..2 }
end
