class Backend::Credit < ViggoRecord
  enum payment_method: [:paypal, :wire_transfer, :credit_card, :refound, :other]

  def customer
    Customer.find(self.customer_id)
  end
end
