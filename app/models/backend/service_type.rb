class Backend::ServiceType < ApplicationRecord

	validates :name, presence: true
end
