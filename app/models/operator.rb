class Operator < ViggoRecord

  self.primary_key = 'id'
  include ConversorHelper
  # => Devise configuration
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :validatable,
         :lockable,
         :timeoutable,
         :trackable


  def default_berry_permissions
    return {
      'services.list'                   => true,
      'services.add'                    => true,
      'services.edit'                   => false,
      'services.delete'                 => false,
      'monitorists.list'                 => true,
      'monitorists.add'                  => false,
      'monitorists.edit'                 => false,
      'monitorists.delete'               => false,
      'units.add'                        => true,
      'units.list'                       => true,
      'serviceRecords.list'                 => true,
      'serviceRecords.add'                  => false,
      'serviceRecords.edit'                 => false,
      'serviceRecords.delete'               => false,

    }
  end

  def form_berry_permissions
    permissions = Hash.new

    self.default_berry_permissions.each do |key, value|
      if !self.berry_permissions[key].nil?
        permissions[key] = eval(self.berry_permissions[key])
      else
        permissions[key] = false
      end
    end

    return permissions
  end

  def parsed_berry_permissions
    return convert_hash_list(self.form_berry_permissions)
  end

  def rates
    Backend::Rate.find_by(customer_id: self.customer_id)
  end

  def available_credits
    credits = 0.0
    Backend::Credit.where(customer_id: self.customer_id, billed: false).each do |credit|
      credits += credit.ammount
      credits -= credit.billed_ammount
    end

    credits
  end
end
