class CheckServicesJob < ApplicationJob
  queue_as :default

  def perform(*args)
  	
    services = Backend::Service.find_by_sql("SELECT * FROM backend_services WHERE deleted = false AND activated = true")
    # return render json: services
    services.each do |service|    ## Recorre cada servicio
      time = 0
      days = 0
      limit = 0
      units = []
      # return render json: Time.at(service[:created_at])
      @operator = Operator.find_by(id: service[:operator_id])
      service.units.each do |unit|
        units[units.length] = unit.wialon_name
      end
      if service[:frequency_time] > 0   ## Si es por tiempo
        if service[:time_type] == 1 ## Hours
          time = Time.now.to_i - Time.at(service[:activated_at]).to_i  ## Tiempo actual - el tiempo que se activo el servicio
          time = time / 3600   ## Se convierte a horas

          frequency_time = service[:frequency_time].hours.to_i / 3600  ## Se convierte a horas

          # return render json: time
          limit = (service[:frequency_time].hours.to_i - 24.hours.to_i) / 3600  ## Rango de tiempo = la frequencia menos 24 horas
          # return render json: limit
          if time >= limit && time < (service[:frequency_time].hours.to_i / 3600) ## Si esta dentro del rango envie correo
            # Send mail 
            # message = "Dentro de " + (frequency_time - time).to_s  + " horas se activará el servicio " + service[:name] + " para las unidades " + units.to_s 
            # return render json: message
            time_left = frequency_time - time
            ServiceMailer.send_mail(units, @operator, service[:name], time_left, "time").deliver_now
          end
          if time >= frequency_time  ## Si el tiempo es mayor o igual a la establecida, se activate el servicio
            ServiceActivatedMailer.send_mail(units, @operator, service[:name]).deliver_now
            service.update({activated_at: Time.now})    ## Se actualizan los datos
            update_data(service[:id])
            create_service_record(service[:name], service.units)
          end
        else
          ## Days
          hours = Time.now.to_i - Time.at(service[:activated_at]).to_i  ## Tiempo actual - Tiempo activado del servicio
          days = hours / (60 * 60 * 24)  ## Se convierte a dias

          frequency_time = service[:frequency_time].days.to_i / (60 * 60 * 24) ## Frecuencia a dias

          limit = (service[:frequency_time].days.to_i - 1.day.to_i) / (60 * 60 * 24)  ## Limite: frecuencia - 1 dia
          # return render json: frequency_time * 24
          if days >= limit && days < frequency_time  ## Si esta dentro del rango enviar correo (dia >= limite y dia < la frecuencia)
            # Send mail 
            # message = "Dentro de " + (frequency_time * 24 - hours / 3600).to_s  + " horas se activará el servicio " + service[:name] + " para las unidades " + units.to_s 
            # return render json: message
            time_left = frequency_time * 24 - hours / 3600
            ServiceMailer.send_mail(units, @operator, service[:name], time_left, "time").deliver_now
          end
          if days >= frequency_time  ## Si pasa se activa el servicio y se actualizan
            ServiceActivatedMailer.send_mail(units, @operator, service[:name]).deliver_now
            service.update({activated_at: Time.now})
            update_data(service[:id])
            create_service_record(service[:name], service.units)
          end
          # return render json: limit 
        end
      end

      if service[:frequency_km] > 0  ## Si es por kilometraje

        # units_services = Backend::MqttUnitsService.where(service_id: service[:id])
        units_services = Backend::MqttUnitsService.find_by_sql("SELECT * FROM backend_mqtt_units_services WHERE service_id = '#{service[:id]}'")
        service_mileage = 0
        actual_mileage = 0
        kunit = []
        units_services.each do |relation|
          
          service_mileage = relation['mileage'].to_f
          message = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                      WHERE ident = '862894022409166' AND mileage > 0
                                                      ORDER BY date_receive DESC LIMIT 1;")
          ############## Atento
          message2 = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                      WHERE ident = '868789020849468' AND mileage > 0
                                                      ORDER BY date_receive DESC LIMIT 1;")

          actual_engine_hour = message2[0]['sensors']['hour_meter_count'].to_f
          ##############
          actual_mileage = message[0]['mileage'].to_f
          unit = Backend::MqttUnit.where(id: relation['mqtt_unit_id'].to_i)
          unit.each do |unit|
            kunit[kunit.length] = unit.wialon_name
          end
          # return render json: messege
          if (actual_mileage - service_mileage) >= (service[:frequency_km] - 2) && (actual_mileage - service_mileage) < (service[:frequency_km])
            #send_mail
            
            km_left = service[:frequency_km] - (actual_mileage - service_mileage)
            ServiceMailer.send_mail(kunit, @operator, service[:name], km_left, "km").deliver_now
          end
          if (actual_mileage - service_mileage) >= service[:frequency_km]
            relation.update({mileage: actual_mileage.to_f, engine_hours: actual_engine_hour.to_f})
            service.update({activated_at: Time.now})
            ServiceActivatedMailer.send_mail(kunit, @operator, service[:name]).deliver_now
            create_service_record(service[:name], unit)
          end
        end
      end

      if service[:frequency_engine] > 0
        units_services = Backend::MqttUnitsService.find_by_sql("SELECT * FROM backend_mqtt_units_services WHERE service_id = '#{service[:id]}'")
        service_engine_hour = 0
        actual_engine_hour = 0
        kunit = []
        units_services.each do |relation|
          service_engine_hour = relation['engine_hours'].to_f
          message = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                      WHERE ident = '868789020849468' AND mileage > 0
                                                      ORDER BY date_receive DESC LIMIT 1;")
          ################# Atento
          message2 = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                      WHERE ident = '862894022409166' AND mileage > 0
                                                      ORDER BY date_receive DESC LIMIT 1;")
          actual_mileage = message2[0]['mileage'].to_f
          ##################
          actual_engine_hour = message[0]['sensors']['hour_meter_count'].to_f
          unit = Backend::MqttUnit.where(id: relation['mqtt_unit_id'].to_i)
          unit.each do |unit|
            kunit[kunit.length] = unit.wialon_name
          end
          if (actual_engine_hour - service_engine_hour) >= (service[:frequency_engine] - 10) && (actual_engine_hour - service_engine_hour) < service[:frequency_engine]
            
            engine_hours_left = service[:frequency_engine] - (actual_engine_hour - service_engine_hour)
            ServiceMailer.send_mail(kunit, @operator, service[:name], engine_hours_left, "engine").deliver_now
          end
          if (actual_engine_hour - service_engine_hour) >= service[:frequency_engine]
            relation.update({engine_hours: actual_engine_hour.to_f, mileage: actual_mileage.to_f})
            service.update({activated_at: Time.now})
            ServiceActivatedMailer.send_mail(kunit, @operator, service[:name]).deliver_now
            create_service_record(service[:name], unit)
          end
        end
      end
    end
  end

  def create_service_record(name, units)
    units.each do |unit|
      service_record = Backend::ServiceRecord.new
      service_record.name = name
      service_record.state = 1
      service_record.unit_id = unit.id
      service_record.date = Time.now
      service_record.operator_id = @operator.id
      service_record.customer_id = @operator.customer_id
      service_record.save
    end
  end

  def update_data(service_id)
    units_services = Backend::MqttUnitsService.find_by(service_id: service_id)

    message = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                WHERE ident = '862894022409166' AND mileage > 0
                                                ORDER BY date_receive DESC LIMIT 1;")
    actual_mileage = message[0]['mileage'].to_f

    message2 = MqttMessage.find_by_sql("SELECT * FROM mqtt_messages
                                                  WHERE ident = '868789020849468' AND mileage > 0
                                                  ORDER BY date_receive DESC LIMIT 1;")
    actual_engine_hour = message2[0]['sensors']['hour_meter_count'].to_f
    units_services.update({mileage: actual_mileage.to_f, engine_hours: actual_engine_hour.to_f })
  end
end
