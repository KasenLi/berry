class Frontend::UsersController < Frontend::ApplicationController
  def new
  end

  def create
    @user = Operator.find_by(email: operator_params[:email])
    if @user
      if @user.valid_password?(operator_params[:password])
        if @user.permissions[:gcc] == 'true'
          return render json: { error: 'authentication.errors.without_permissions' }, status: :unprocessable_entity
        end
        if true
          sign_in(@user, scope: :operator)
          return render json: { login: true }, status: :ok
        else
          return render json: { error: 'authentication.errors.without_rates' }, status: :unprocessable_entity
        end
      else
        return render json: { error: 'authentication.errors.invalid_password' }, status: :unprocessable_entity
      end
    end

    @user = User.find_by(email: operator_params[:email])
    if @user
      if @user.valid_password?(operator_params[:password])
        sign_in(@user, scope: :user)
        return render json: { login: true }, status: :ok
      else
        return render json: { error: 'authentication.errors.invalid_password' }, status: :unprocessable_entity
      end
    end

    return render json: { error: 'authentication.errors.not_found' }, status: :unprocessable_entity
  end

  private
    def operator_params
      params.require(:operator).permit(:email, :password)
    end
end
