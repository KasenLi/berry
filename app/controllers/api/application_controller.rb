class Api::ApplicationController < ActionController::API
	def integrations
		case params[:app]
		when 'service'
			CheckServicesJob.perform_now
		end
		return render json: { executed: :ok } 
	end

	def index
    return render json: { message: "Welcome to Berry API", hostname: request.host_with_port }
  end
end