import loginRoutes from '@/routes/login'
import maintRoutes from '@/routes/maintenances'
import operationRoutes from '@/routes/operations'
import NotFound from '@/pages/NotFound.vue'
import homeRoutes from '@/routes/home'

const routes = [
  {
    path: '/',
    redirect: '/Main/Home'
  },
  homeRoutes,
  loginRoutes,
  ...maintRoutes,
  ...operationRoutes,
  {
    path: '*',
    component: NotFound
  }
]

export default routes
