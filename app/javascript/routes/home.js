import FrontendLayout from '@/layouts/Frontend.vue'
import HomePage from '@/pages/Home.vue'

export default {
	path: '/Main',
	component: FrontendLayout,
	children: [	
		{
			path: 'Home',
			name: 'home',
			components: { default: HomePage }
		}
	]
}
