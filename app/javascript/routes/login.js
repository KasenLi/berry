import LoginLayout from '@/layouts/Login.vue'
import LoginPage from '@/pages/Login.vue'

export default {
  path: '/Sessions',
  component: LoginLayout,
  children: [
    {
      path: 'New',
      name: 'login',
      components: { default: LoginPage }
    }
  ]
}
