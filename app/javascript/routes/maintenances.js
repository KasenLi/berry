// Main components
import DashboardLayout from '@/layouts/Maintenances.vue'
import DashboardPage from '@/pages/Maintenances.vue'
import ServicesPage from '@/pages/Services.vue'
import ServiceRecordsPage from '@/pages/ServiceRecords.vue'
import WorkshopsPage from '@/pages/Workshops.vue'

export default [
	{
		path: '/Maintenances',
		redirect: '/Maintenances/Home'
	},
	{
		path: '/Maintenances',
		component: DashboardLayout,
		children: [
			{
				path: 'Home',
				name: 'maintenancesHome',
				components: { default: DashboardPage }
			},
			{
				path: 'Services',
				name: 'services',
				components: { default: ServicesPage }
			},
			{
				path: 'ServiceRecords',
				name: 'serviceRecords',
				components: { default: ServiceRecordsPage }
			},
			{
				path: 'Workshops',
				name: 'workshops',
				components: { default: WorkshopsPage }
			}
		]
	}
]