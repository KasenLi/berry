// Main components
import DashboardLayout from '@/layouts/Operations.vue'
import DashboardPage from '@/pages/Operations.vue'
import UnitsPage from '@/pages/Units.vue'
import MonitoristsPage from '@/pages/Monitorists.vue'

export default [
	{
		path: '/Operations',
		redirect: 'Operations/Home'
	},
	{
		path: '/Operations',
		component: DashboardLayout,
		children: [
			{
				path: 'Home',
				name: 'operationsHome',
				components: { default: DashboardPage }
			},
			{
				path: 'Units',
				name: 'units',
				components: { default: UnitsPage }
			},
			{
				path: 'Monitorists',
				name: 'monitorists',
				components: { default: MonitoristsPage }
			},
		]
	}
]