export default {
  inputColor (state) {
    return state.dark ? 'white' : 'primary'
  }
}
