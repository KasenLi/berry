export default {
  mainDomain: 'cms.goffice.io',
  user: {
    id: 0,
    username: '',
    apiToken: '',
    sidebarStatus: false
  },
  whitelabels: [],
  countries: [],
  services: [],
  serviceRecords: [],
  linkedWialonAccounts: [],
  units: [],
  serviceTypes: [],
  monitorists: [],
  workshops: [],
  parameters: [],
  buttonState: false,
  whitelabel: {},
  defaultWhitelabel: {},
  isLocal: process.env.NODE_ENV === 'development'
}
