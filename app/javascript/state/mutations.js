export default {
  toggleDark (state) {
    state.dark = !state.dark
  },

  updateToken (state, payload) {
    state.user.apiToken = payload
  },

  setUserInformation (state, payload) {
    state.user = payload
    state.user.whitelabels = []
  },

  toggleSidebar (state, payload = undefined) {
    state.user.sidebarStatus = (payload === undefined ? !state.user.sidebarStatus : payload)
  },

  updateServices (state, payload) {
    state.services = []
    state.services = payload
  },

  updateServiceRecords (state, payload) {
    state.serviceRecords = []
    state.serviceRecords = payload
  },
  
  updateUnits (state, payload) {
    state.units = []
    state.units = payload
  },

  updateWhitelabels (state, payload) {
    state.whitelabels = []
    state.whitelabels = payload
  },

  updateCountries (state, payload) {
    state.countries = []
    state.countries = payload
  },

  updatePlans (state, payload) {
    state.plans = []
    state.plans = payload
  },

  updateWhitelabel (state, payload) {
    state.whitelabel = payload
  },

  updateLinkedWialonAccounts (state, payload) {
    state.linkedWialonAccounts = []
    state.linkedWialonAccounts = payload
  },

  updateServiceTypes (state, payload) {
    state.serviceTypes = []
    state.serviceTypes = payload
  },

  updateMonitorists (state, payload) {
    state.monitorists = []
    state.monitorists = payload
  },

  updateWorkshops (state, payload) {
    state.workshops = []
    state.workshops = payload
  },

  updateParameters (state, payload) {
    state.parameters = []
    state.parameters = payload
  },
  
  setDefaultWhitelabel (state, payload) {
    state.defaultWhitelabel = payload
  },

  toggleAlert (state, payload) {},

  setButtonState (state, payload) {
    state.buttonState = payload
  },
}
