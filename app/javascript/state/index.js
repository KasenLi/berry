import state from '@/state/state'
import mutations from '@/state/mutations'
import getters from '@/state/getters'
import actions from '@/state/actions'

export {
  state,
  mutations,
  getters,
  actions
}
