import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'

let client = new ApolloClient({
  uri: '/graphql'
})

export default {

  loadUserInformation (context, payload) {
    if (payload.apiToken === null) {
      return false
    }

    context.commit('setButtonState', true)

    client.query({
      query: gql`query ($apiToken:String!){
        auth(apiToken: $apiToken) {
          status,
          errors,
          auth {
            __typename
            ...on Operator {
              id
              name
              email
              apiToken
              permissions: formBerryPermissions
            }
          }
        }
      }`,
      variables: {
        apiToken: payload.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let data = response.data.auth

      if (data.status) {
        let sidebarStatus = false
        sessionStorage.setItem('sidebarStatus', false)

        if (sessionStorage.getItem('sidebarStatus') !== undefined) {
          if (sessionStorage.getItem('sidebarStatus') === 'true') {
            sidebarStatus = true
            sessionStorage.setItem('sidebarStatus', true)
          } else {
            sidebarStatus = false
            sessionStorage.setItem('sidebarStatus', false)
          }
        }

        context.commit('setUserInformation', {
          ...data.auth,
          sidebarStatus: sidebarStatus
        })
        if (data.auth.__typename === 'User') {
          // this.$store.dispatch('getPlans')
          // this.$router.push('/Team')
        } else {
          context.dispatch('loadCustomerData')
          payload.router.push('/Operations')
        }
        context.commit('setButtonState', false)
        return true
      } else {
        context.commit('setButtonState', false)
        return false
      }
    }).catch(error => {
      context.commit('setButtonState', false)

      context.commit('toggleAlert', {
        type: 'error',
        message: payload.i18n.t('errors.internalError'),
        options: {
          left: false,
          right: false,
          top: false,
          bottom: true
        }
      })
    })
  },

  signOut (context, payload) {
    sessionStorage.removeItem('apiToken')
    context.commit('setUserInformation', {
      id: 0,
      username: '',
      apiToken: '',
      sidebarStatus: false,
    })

    payload.router.push('/Sessions/New')
  },

  async loadCustomerData (context) {
    context.dispatch('getUnits')
    context.dispatch('getServices')
    context.dispatch('getServiceRecords')
    context.dispatch('getLinkedWialonAccounts')
    context.dispatch('getServiceTypes')
    context.dispatch('getMonitorists')
    context.dispatch('getWorkshops')
    context.dispatch('getParameters')
  },

  async toggleSidebar (context, payload) {
    context.commit('toggleSidebar', payload)
    sessionStorage['sidebarStatus'] = payload
      

    // await client.mutate({
    //   mutation: gql`mutation ($apiToken:String!, $sidebarStatus:Boolean!) {
    //     toggleSidebar(input: {
    //       apiToken:$apiToken,
    //       sidebarStatus:$sidebarStatus
    //     }) {
    //       status
    //       sidebarStatus
    //     }
    //   }`,
    //   variables: {
    //     apiToken: context.state.user.apiToken,
    //     sidebarStatus: payload
    //   },
    //   fetchPolicy: 'no-cache'
    // }).then(response => {
    //   let { status, sidebarStatus } = response.data.toggleSidebar
    //   context.commit('toggleSidebar', sidebarStatus)
    //   sessionStorage['sidebarStatus'] = sidebarStatus
      
    //   if (!status) {
    //     context.commit('toggleAlert', {
    //       type: 'error'
    //     })
    //   }
    // }).catch(error => {
    //   context.commit('toggleAlert', {
    //     type: 'error'
    //   })

    //   console.error(error)
    // })
  },

  resetWhitelabel (context) {
    context.commit('updateWhitelabel', context.state.defaultWhitelabel)
  },

  getWhitelabel (context, payload) {
    client.query({
      query: gql`query ($cname:String!) {
        wl(host:$cname) {
          status
          errors
          whitelabel {
            pageTitle
            color
            logo
            brand
            footerUrl
            footerName
          }
        }
      }`,
      variables: {
        cname: payload
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let { status, whitelabel } = response.data.wl

      if (status === true) {
        context.commit('updateWhitelabel', whitelabel)
      } else {
        context.commit('updateWhitelabel', context.state.defaultWhitelabel)
      }
    }).catch(error => {
      context.commit('toggleAlert', {
        type: 'error'
      })

      console.error(error)
    })
  },

  // async getCountries (context) {
  //   await client.query({
  //     query: gql`{
  //       countries {
  //         id
  //         commonName
  //         flagEmoji
  //       }
  //     }`,
  //     fetchPolicy: 'no-cache'
  //   }).then(response => {
  //     let countries = response.data.countries
  //     context.commit('updateCountries', countries)
  //   }).catch(error => {
  //     context.commit('toggleAlert', {
  //       type: 'error'
  //     })

  //     console.error(error)
  //   })
  // },

  async getServices (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        services(apiToken: $apiToken) {
          status,
          errors,
          services {
            id
            name
            frequency
            unitsCount
            frequencyKm
            frequencyTime
            frequencyEngine
            timeType
            activated
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, services } = response.data.services
      if (status) {
        context.commit('updateServices', services)
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getServiceRecords (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        serviceRecords(apiToken: $apiToken) {
          status,
          errors,
          serviceRecords {
            id
            name
            unitName
            state
            date
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, serviceRecords } = response.data.serviceRecords
      if (status) {
        context.commit('updateServiceRecords', serviceRecords)
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getUnits (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        units(apiToken: $apiToken) {
          status,
          errors,
          units {
            id
            name: wialonName
            ident: wialonImei
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, units } = response.data.units
      if (status) {
        context.commit('updateUnits', units)
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getLinkedWialonAccounts (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        linkedWialonAccounts(apiToken: $apiToken) {
          status,
          errors,
          linkedWialonAccounts {
            id
            username
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, linkedWialonAccounts } = response.data.linkedWialonAccounts
      if (status) {
        console.log(response.data)
        context.commit('updateLinkedWialonAccounts', linkedWialonAccounts)
      } else {
        console.log(response.data)
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getServiceTypes (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        serviceTypes(apiToken: $apiToken) {
          status,
          errors,
          serviceTypes {
            id
            name
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, serviceTypes } = response.data.serviceTypes
      if (status) {
        console.log(response.data)
        context.commit('updateServiceTypes', serviceTypes)
      } else {
        console.log(response.data)
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getMonitorists (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        monitorists(apiToken: $apiToken) {
          status,
          errors,
          monitorists {
            id
            name
            username
            email
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, monitorists } = response.data.monitorists
      if (status) {
        context.commit('updateMonitorists', monitorists)
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getWorkshops (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        workshops(apiToken: $apiToken) {
          status,
          errors,
          workshops {
            id
            name
            unitName
            state
            dateIn
            dateOut
            service
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, workshops } = response.data.workshops
      if (status) {
        context.commit('updateWorkshops', workshops)
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async getParameters (context) {
    await client.query({
      query: gql`query ($apiToken:String!){
        parameters(apiToken: $apiToken) {
          status,
          errors,
          parameters {
            id
            esName,
            measure,
            chartType,
            modal,
            extraChart,
            extraChartType,
            icon,
            limLegends
          }
        }
      }`,
      variables: {
        apiToken: context.state.user.apiToken
      },
      fetchPolicy: 'no-cache'
    }).then(response => {
      let {status, errors, parameters } = response.data.parameters
      if (status) {
        context.commit('updateParameters', parameters)
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: errors.join(', ')
        })
      }
    })
  },

  async deleteItem (context, payload) {
    await client.mutate({
      mutation: gql`mutation ($id:Int!, $apiToken:String!){
        ${payload.mutation.graphql} (input:{
          id:$id,
          apiToken:$apiToken
        }) {
          status
          errors
          ${payload.object.name}${payload.object.fields}
        }
      }`,
      variables: payload.variables
    }).then(response => {
      let data = response.data[payload.mutation.graphql]

      if (data.status) {
        context.commit(payload.mutation.vuex, data[payload.object.name])
      } else {
        context.commit('toggleAlert', {
          type: 'error',
          message: data.errors.join(', ')
        })
      }
    }).catch(error => {
      context.commit('toggleAlert', {
        type: 'error'
      })

      console.error(error)
    })
  },
}
