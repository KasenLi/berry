export default {
  actions: {
    back: 'Volver',
    edit: 'Editar',
    save: 'Guardar',
    error: 'error ha',
    errors: 'errores han',
    submit: 'Enviar',
    while_create: 'impedido que se guardaba el registro',
    while_detroy: 'impedido que se eliminaba el registro',
    while_update: 'impedido que se actualizaba el registro',
    reset: "Reiniciar procedimiento",
    next: "Siguiente",
    search: "Buscar registros",
    saved: "Se ha guardado exitosamente",
    editItem: "Editar {name}",
    cancel: "Cancelar",
    close: "Cerrar ventana",
    ok: "Continuar",
    createItem: "Crear {name}",
    discardChanges: "Ignorar cambios",
    saveChanges: "Guardar cambios"
  },

  authentication: {
    login: 'Iniciar sesión',
    logout: 'Cerrar sesión',
    sign_up: 'Regístrate ahora',
    help: '¿Necesitas ayuda?',
    errors: {
      without_permissions: 'No posees los permisos suficientes para dicha acción',
      without_rates: 'Lo sentimos, pero su período de prueba ha vencido, si así lo desea, contáctenos para activar sus tarifas y pueda seguir utilizando Goffice. Posee 30 días para realizar esta activación, posterior a esta fecha, su información será eliminada automáticamente.',
      unconfirmed: 'Lo sentimos, pero su cuenta no ha sido confirmada, por favor verifique en su buzón de correo no deseado para realizar la confirmación',
      not_found: 'Lo sentimos, pero no hemos podido encontrar el correo electrónico en nuestros registros',
      invalid_password: 'Lo sentimos, tu combinación correo electsrónico y contraseña es inválida'
    },
    problems: {
      email: '¿Has olvidado tu correo electrónico?',
      password: '¿Has olvidado tu contraseña?'
    }
  },

  login: {
    success: "Hemos detectado una sesión abierta, te redirigieremos a la aplicación",
    signUpSuccess: '¡Bienvenido a Goffice! en su correo recibirá un enlace de confirmación',
    back: 'Volver al inicio de sesión',
    signUp: '¡Regístrate!',
    username: 'Nombre de usuario',
    password: 'Contraseña',
    login: 'Iniciar sesión',
    logout: 'Cerrar sesión',
    forgot: 'He olvidado mi contraseña'
  },

  permissions: {
    unauthorized: 'No tienes los permisos necesarios.'
  },

  app: {
    slogan: '¡Mantén tu vehículo!',
    title: 'Berry',
    title_long: 'Berry',
    titleShort: 'Berry',
    title2: 'Berry',
    email: 'sales@goldenmcorp.com',
    brand: 'Berry',
    operations: 'Operaciones',
    maintenances: 'Mantenimiento'
  },

  copyright: {
    company: 'UJAP',
    website: 'https://ujap.com',
    all_rights: 'Todos los derechos reservados'
  },

  pages: {
    home: 'Inicio',
    maintenancesHome: 'Inicio',
    operationsHome: 'Inicio',
    serviceRecords: 'Registro de Servicios',
    services: 'Servicios',
    units: 'Unidades',
    monitorists: 'Monitoristas',
    workshops: 'Servicio Taller'
  },

  home: { 
    title: 'Monitoreo activo de eventos',
    operationsMonitor: 'Dashboard',
    maintenancesMonitor: 'Mantenimiento'
  },

  dual_list: {
    list1: 'Lista 1',
    list2: 'Lista 2',
    findIn: 'Buscar en',
    noData: 'No hay registros disponibles',
    nextAll: 'mdi mdi-chevron-double-right',
    prevAll: 'mdi mdi-chevron-double-left'
  },

  errors: {
    invalid_format: 'Lo sentimos, pero el archivo cargado no posee el formato válido, por favor verifique el archivo que cargó',
    wialon_login: 'Lo sentimos, pero el inicio de sesión con Wialon ha fallado, por favor verifique sus credenciales en el CMS e inténtelo de nuevo',
    wialon_get_units: 'Lo sentimos, pero algo salió mal extrayendo la información de Wialon, por favor inténtelo de nuevo',
    generic_message: 'Lo sentimos, pero algo ha salido mal',
    conditions: 'Debes aceptar los términos y condiciones',
    404: 'Lo sentimos, pero la página que intentas acceder no existe. <br /> Asegúrate de haber escrito la ruta correctamente.',
    permissions: 'No posees los permisos necesarios para acceder a dicho módulo',
    invalidFields: 'Lo sentimos, pero no pudimos procesar el registro por errores en los campos',
    internalError: 'Lo sentimos, pero algo salió mal',
    pageNotFound: 'Oh vaya, parece que la ruta que intentas acceder no existe',
    tryAgain: '¡inténtalo de nuevo!',
    authenticationRequired: "Primero debe iniciar sesión para utilizar la aplicación"
  },

  header: {
    speedings: 'Excesos de velocidad',
    misc: 'Miscelaneo',
    units_status: 'Estado del equipo',
    operations: 'Dashboard',
    maintenances: 'Mantenimiento',
    units: 'Unidades'
  },

  helpers: {
    upload_file: 'Cargue un archivo para continuar',
    true: 'Si',
    false: 'No',
    empty: 'Campo sin rellenar',
    not_available: 'No disponible',
    required: 'Campo requerido',
    optional: 'Campo opcional',
    emptyItem: "No proporcionado",
    selectAll: "Seleccionar todo",
    deselectAll: "Deseleccionar todo",
    enabled: "Habilitado",
    disabled: "Deshabilitado",
    generated: 'Campo generado automaticamente',
    actions: 'Acciones',
    notFound: "Lo sentimos, pero no hemos encontrado registros con la búsqueda {search}"
  },

  sidebar: {
    customers: 'Clientes',
    home: 'Inicio',
    home_operations: 'Inicio',
    home_units_status: 'Estado del equipo',
    home_miscellaneous: 'Elementos varios',
    home_speedings: 'Excesos de velocidad',
    support: '¿Necesitas ayuda?',
    'units/status': 'Estado del equipo',
    speedings: 'Excesos de velocidad',
    units_status: 'Estado del equipo',
    monitorists: 'Monitoristas',
    home_maintenances: 'Inicio',
    units: 'Unidades',
    services: 'Servicios',
    intervals: 'Intervalos'
  },

  table: {
    empty_rows: 'Lo sentimos, no tenemos registros disponibles',
    showing: 'Mostrando',
    of: 'de',
    rows: 'registros',
    row: 'registro',
    find: 'Buscar registro',
    per_page: 'por página',
    rows_per_page: 'Registros por página',
    loaded: 'cargados',
    confirm_delete: '¿Está seguro que desea eliminar el registro?'
  },

  units: {
    wialonName: 'Nombre de la unidad',
    wialon_plate: 'Placa de la unidad',
    wialonImei: 'ID único de la unidad',
    linked_wialon_account_id: 'Origen de la unidad',
    titleIndex: 'Unidades',
    titleShow: 'Unidad {name}',
    titleNew: 'Importar desde Wialon',
    objectName: 'Unidad'
  },

  monitorists: {
    email: 'Correo electrónico',
    name: 'Nombre',
    username: "Nombre de usuario",
    identifier: 'Identificador único',
    units: 'Unidades',
    phone: 'Teléfono de contacto',
    linkedAccount: 'Cuenta de Wialon vinculada',
    wialonAccount: 'Usuario de Wialon',
    password: 'Contraseña',
    title_index: 'Monitoristas',
    titleNew: 'Nuevo monitorista',
    titleShow: 'Monitorista {name}',
    titleEdit: 'Editar monitorista {name}',
    basicConfiguration: "Configuración básica",
    objectName: 'Monitorista'
  },

  serviceRecords: {
    name: 'Nombre del servicio',
    unit: 'Unidad',
    pickDate: 'Seleccione una fecha',
    date: 'Fecha',
    state: 'Estado',
    type: 'Seleccione un servicio creado o cree uno nuevo',
    type_name: 'Servicio',
    is_save: '¿Desea guardar el nombre del servicio?',
    title_index: 'Servicios',
    titleNew: 'Nuevo servicio',
    titleShow: 'Servicio {name}',
    titleEdit: 'Editar servicio {name}',
    objectName: 'Registro de servicio'
  },

  services: {
    name: 'Nombre del servicio',
    unit: 'Unidades',
    units: 'Unidades asignadas',
    frequencyType: 'Tipo de frecuencia',
    frequency: 'Frecuencia',
    frequencyKm: 'Frecuencia en KM',
    frequencyTime: 'Frecuencia en tiempo',
    frequencyEngine: 'Frecuencia por horas de motor',
    timeType: 'Tiempo',
    titleIndex: 'Servicios',
    titleNew: 'Nuevo servicio',
    titleShow: 'Servicio {name}',
    titleEdit: 'Editar servicio {name}',
    objectName: 'Servicio',
    activated: 'Activado',
    deactivation_title: 'Desactivación de servicio',
    want_to_deactivate: '¿Está seguro que desea desactivar el servicio?'

  },

  operations: {
    date_start: "Fecha inicial",
    date_end: "Fecha final",
    today: "Día",
    week: "Semana",
    month: "Mes",
    custom: "Personalizado"
  },

  workshops: {
    name: 'Nombre del taller',
    service: 'Servicio',
    unit: 'Unidad',
    pickDate: 'Seleccione una fecha',
    dateIn: 'Fecha ingreso',
    dateOut: 'Fecha salida',
    state: 'Estado',
    type: 'Seleccione un servicio creado o cree uno nuevo',
    type_name: 'Servicio',
    is_save: '¿Desea guardar el nombre del servicio?',
    title_index: 'Servicios',
    titleNew: 'Nuevo servicio en Taller',
    titleShow: 'Servicio taller {name}',
    titleEdit: 'Editar servicio taller {name}',
    objectName: 'Servicio de taller'
  },

}
