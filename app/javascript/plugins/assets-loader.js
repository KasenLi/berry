const assetsLoader = {}

let assets = {
  favicon: require('../../assets/images/favicon.png'),
  background: require('../../assets/images/bg.jpg')
}
assetsLoader.install = function(Vue){
  Vue.prototype.$assetsLoader = new Vue({
    data () {
      return {
        ...assets,
        avatar: require('../../assets/images/default-avatar.png'),
        logos: {
          gcc: {
            normal: require('../../assets/images/logos/gcc-normal.png'),
            invert: require('../../assets/images/logos/gcc.png')
          },
          alpha: {
            normal: require('../../assets/images/logos/alpha-normal.png'),
            invert: require('../../assets/images/logos/alpha.png')
          },
          mfence: {
            normal: require('../../assets/images/logos/mfence-normal.png'),
            invert: require('../../assets/images/logos/mfence.png')
          },
          signaly: {
            normal: require('../../assets/images/logos/signaly-normal.png'),
            invert: require('../../assets/images/logos/signaly.png')
          },
          instora: {
            normal: require('../../assets/images/logos/instora-normal.png'),
            invert: require('../../assets/images/logos/instora.png')
          },
          gosapp: {
            normal: require('../../assets/images/logos/gosapp-normal.png'),
            invert: require('../../assets/images/logos/gosapp.png')
          },
        }
      }
    }
  })
}

export default assetsLoader