import 'es6-promise/auto'
import '@/plugins/polyfills'
import GlobalComponents from '@/plugins/global-components'

export default {
  install (Vue) {
    Vue.use(GlobalComponents)
  }
}
