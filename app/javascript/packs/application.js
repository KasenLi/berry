// Main dependencies
import Vue from 'vue'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import AssetsLoader from '@/plugins/assets-loader'
// import GraphQL from '@/plugins/graphql'
import VueMouseParallax from 'vue-mouse-parallax'
import VueApollo from 'vue-apollo'

import 'babel-polyfill'

// Apollo client
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

// Plugins dependencies
import messages from '@/locales'
import routes from '@/routes'
import { state, mutations, getters, actions } from '@/state'

// Vuetify Locales
import es from 'vuetify/es5/locale/es'
import en from 'vuetify/es5/locale/en'

// Template
import App from '@/App.vue'

// Components
import '@mdi/font/css/materialdesignicons.css'
import MaterialCard from '@/components/material/Card.vue'
import 'echarts'
import DatetimePicker from 'vuetify-datetime-picker'

// Components setup
Vue.use(VueI18n)
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueMouseParallax)
Vue.use(VueApollo)
Vue.use(AssetsLoader)
Vue.use(DatetimePicker)
// Vue.use(GraphQL)
Vue.use(Vuetify, {
  lang: {
    locales: { es, en },
    current: 'es'
  },

  iconfont: 'mdi',

  options: {
    customProperties: true
  },

  theme: {
    primary: '#00a5e8', // Change that into /stylesheets/material-dashboard/_variables.scss
    secondary: '#2d5db5',
    accent: '#d8a42a'
  }
})
Vue.use(Snotify, {
  toast: {
    position: SnotifyPosition.rightTop
  }
})

// => Esential plugins
// Apollo setup
// HTTP connexion to the API
const uri = (process.env.NODE_ENV === 'development' ? 'http://localhost:3000/graphql' : `https://${window.location.hostname}/graphql`)

const httpLink = new HttpLink({
  uri: uri
})

const cache = new InMemoryCache({
  fetchPolicy: 'no-cache'
})

const apolloClient = new ApolloClient({
  link: httpLink,
  cache
})

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

// VueI18n setup
const i18n = new VueI18n({
  locale: 'es',
  messages
})

// VueRouter setup
const router = new VueRouter({
  routes
})

// Vuex setup
const store = new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})

Vue.component('material-card', MaterialCard)

document.addEventListener('DOMContentLoaded', () => {
  const el = document.body.appendChild(document.createElement('hello'))
  /* eslint-disable no-new */
  new Vue({
    el,
    store,
    router,
    i18n,
    apolloProvider,
    render: h => h(App),
    created () {
      this.$store.subscribe(mutation => {
        if (mutation.type === 'updateWhitelabel') {
          let payload = mutation.payload

          document.title = payload.pageTitle
          this.$vuetify.theme.primary = payload.color
        }
      })

      const defaultWhitelabbel = {
        pageTitle: this.$i18n.t('app.title'),
        brand: this.$i18n.t('app.brand'),
        logo: require('../../assets/images/favicon.png'),
        footerUrl: this.$i18n.t('copyright.website'),
        footerName: this.$i18n.t('copyright.company'),
        color: '#00a5e8'
      }

      this.$store.commit('setDefaultWhitelabel', defaultWhitelabbel)
      this.$store.commit('updateWhitelabel', defaultWhitelabbel)

      if (window.location.hostname !== 'cms.goffice.io' && process.env.NODE_ENV !== 'development') {
        this.$store.dispatch('getWhitelabel', window.location.hostname)
      }
      //this.$store.dispatch('getCountries')
      console.log('%c MAGIC!!', 'color: #754c24; font-weight: bold; font-size: 3em; text-shadow: 0px 0px 10px #754c24;')
      console.log('https://cms.goffice.io/hello%20world')
    }
  })
})
