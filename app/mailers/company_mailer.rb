class CompanyMailer < ApplicationMailer
  default from: 'Support | Viggo by Golden M <no-reply@goffice.io>'

  def send_account(password, company)
    @password = password
    @company = company
    mail(to: @company.email, subject: "Welcome to Viggo Berry!")
  end
end
