class MonitoristMailer < ApplicationMailer
	default from: 'Support | Viggo by Golden M <no-reply@goffice.io>'

  def send_account(password, monitorist)
    @password = password
    @monitorist = monitorist
    mail(to: @monitorist.email, subject: "Welcome to Viggo Berry!")
  end
end
