class ServiceMailer < ApplicationMailer
	default from: 'Support | Viggo Berry by Golden M <no-reply@goffice.io>'

	def send_mail(units, operator, service, value, type)
    @units = units
    @operator = operator
    @value = value
    @service = service
    @type = type
    subject = " El Servicio " + @service.to_s + " está a punto de activarse" 
    mail(to: @operator[:email], subject: subject)
  end
end
