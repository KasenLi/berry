class ServiceActivatedMailer < ApplicationMailer
	default from: 'Support | Viggo Berry by Golden M <no-reply@goffice.io>'

	def send_mail(units, operator, service)
    @units = units
    @operator = operator
    @service = service
    subject = " El Servicio " + @service.to_s + " se activó" 
    mail(to: @operator[:email], subject: subject)
  end
end
