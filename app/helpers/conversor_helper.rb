module ConversorHelper
  def convert_hash_list(hast_list)
    element = Hash.new
    hast_list.each do |key, value|
      self.convert_to_dictionary(key, value, element)
    end

    return element
  end

  def convert_to_dictionary(key, value, element)
    if key.include?(".")
      key = key.split(".")

      new_key = Array.new

      key.each.with_index do |k, i|
        if i != 0
          new_key.push(k)
        end
      end

      key = key.first

      if element[key].nil?
        element[key] = Hash.new
      end

      element[key] = self.convert_to_dictionary(new_key.join("."), value, element[key])
    else
      element[key] = value
    end

    element
  end
end
