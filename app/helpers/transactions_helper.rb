module TransactionsHelper
  def calculate_prorate(price, quantity, date = Time.zone.now.day)
    if (date >= 30)
      ammount = price * quantity
    else
      remaining_days = (30 - date)
      price = price / 30
      ammount = (price * remaining_days) * quantity
    end

    return ammount
  end

  def decrease_credits(customer_id, ammount)
    Backend::Credit.where(customer_id: customer_id, billed: false).order(id: :ASC).each do |credit|
    if (credit.ammount - credit.billed_ammount) >= ammount
      credit.billed_ammount += ammount
      ammount = 0
      if (credit.ammount - credit.billed_ammount) == 0
        credit.billed = true
      end

      credit.save
      break
    else

      remaining = (credit.ammount - credit.billed_ammount)
      remaining_ammount = ammount - remaining
      credit.billed_ammount += remaining
      ammount = remaining_ammount
      credit.billed = true
      credit.save
    end
  end
  end
end
