source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.0'

gem 'rails', '~> 5.2.2'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'stylus'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'capistrano-rails', group: :development
gem 'bootsnap', '>= 1.1.0', require: false
gem 'rack-cors'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

## Golden M custom gems

# => Crontabs management
gem 'whenever'

# => Slim template engine
gem 'slim'
gem 'slim-rails'

# => Authentication and permissions
gem 'devise'
gem 'cancancan'

# => Gurtam APIs'
gem 'flespi', '~> 1.0.0'
gem 'wialon', '~> 1.1.4'

# => Mailgun
gem 'mailgun-ruby', '~>1.1.6'

# => MQTT Client
gem 'mqtt'

# => GraphQL && GraphiQL
gem 'graphql'
gem 'graphiql-rails', group: :development