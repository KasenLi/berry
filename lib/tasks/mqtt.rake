namespace :mqtt do
	desc "Start MQTT Broker client"
  task :start, [:server_number] => [:environment] do |task, arg|
    mqtt = MQTTBroker.new
    mqtt.start(arg[:server_number].to_i)
  end
  
	desc "Start MQTT Event client"
	task :event, [:server_number] => [:environment] do |task, arg|
		mqtt = MQTTClient.new
		mqtt.start(arg[:server_number].to_i)
	end
end